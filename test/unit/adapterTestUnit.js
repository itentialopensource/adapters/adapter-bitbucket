/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-bitbucket',
      type: 'Bitbucket',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const Bitbucket = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] Bitbucket Adapter Test', () => {
  describe('Bitbucket Class Tests', () => {
    const a = new Bitbucket(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('bitbucket'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.8.2', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('bitbucket'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('Bitbucket', pronghornDotJson.export);
          assert.equal('Bitbucket', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-bitbucket', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('bitbucket'));
          assert.equal('Bitbucket', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-bitbucket', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-bitbucket', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#deleteAddon - errors', () => {
      it('should have a deleteAddon function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAddon === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putAddon - errors', () => {
      it('should have a putAddon function', (done) => {
        try {
          assert.equal(true, typeof a.putAddon === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAddonLinkers - errors', () => {
      it('should have a getAddonLinkers function', (done) => {
        try {
          assert.equal(true, typeof a.getAddonLinkers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAddonLinkersLinkerKey - errors', () => {
      it('should have a getAddonLinkersLinkerKey function', (done) => {
        try {
          assert.equal(true, typeof a.getAddonLinkersLinkerKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkerKey', (done) => {
        try {
          a.getAddonLinkersLinkerKey(null, (data, error) => {
            try {
              const displayE = 'linkerKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getAddonLinkersLinkerKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAddonLinkersLinkerKeyValues - errors', () => {
      it('should have a deleteAddonLinkersLinkerKeyValues function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAddonLinkersLinkerKeyValues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkerKey', (done) => {
        try {
          a.deleteAddonLinkersLinkerKeyValues(null, (data, error) => {
            try {
              const displayE = 'linkerKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteAddonLinkersLinkerKeyValues', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAddonLinkersLinkerKeyValues - errors', () => {
      it('should have a getAddonLinkersLinkerKeyValues function', (done) => {
        try {
          assert.equal(true, typeof a.getAddonLinkersLinkerKeyValues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkerKey', (done) => {
        try {
          a.getAddonLinkersLinkerKeyValues(null, (data, error) => {
            try {
              const displayE = 'linkerKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getAddonLinkersLinkerKeyValues', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAddonLinkersLinkerKeyValues - errors', () => {
      it('should have a postAddonLinkersLinkerKeyValues function', (done) => {
        try {
          assert.equal(true, typeof a.postAddonLinkersLinkerKeyValues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkerKey', (done) => {
        try {
          a.postAddonLinkersLinkerKeyValues(null, (data, error) => {
            try {
              const displayE = 'linkerKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postAddonLinkersLinkerKeyValues', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putAddonLinkersLinkerKeyValues - errors', () => {
      it('should have a putAddonLinkersLinkerKeyValues function', (done) => {
        try {
          assert.equal(true, typeof a.putAddonLinkersLinkerKeyValues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkerKey', (done) => {
        try {
          a.putAddonLinkersLinkerKeyValues(null, (data, error) => {
            try {
              const displayE = 'linkerKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-putAddonLinkersLinkerKeyValues', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAddonLinkersLinkerKeyValues2 - errors', () => {
      it('should have a deleteAddonLinkersLinkerKeyValues2 function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAddonLinkersLinkerKeyValues2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkerKey', (done) => {
        try {
          a.deleteAddonLinkersLinkerKeyValues2(null, (data, error) => {
            try {
              const displayE = 'linkerKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteAddonLinkersLinkerKeyValues2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAddonLinkersLinkerKeyValues2 - errors', () => {
      it('should have a getAddonLinkersLinkerKeyValues2 function', (done) => {
        try {
          assert.equal(true, typeof a.getAddonLinkersLinkerKeyValues2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkerKey', (done) => {
        try {
          a.getAddonLinkersLinkerKeyValues2(null, (data, error) => {
            try {
              const displayE = 'linkerKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getAddonLinkersLinkerKeyValues2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHookEvents - errors', () => {
      it('should have a getHookEvents function', (done) => {
        try {
          assert.equal(true, typeof a.getHookEvents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHookEventsSubjectType - errors', () => {
      it('should have a getHookEventsSubjectType function', (done) => {
        try {
          assert.equal(true, typeof a.getHookEventsSubjectType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subjectType', (done) => {
        try {
          a.getHookEventsSubjectType(null, (data, error) => {
            try {
              const displayE = 'subjectType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getHookEventsSubjectType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugHooks - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugHooks function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugHooks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugHooks(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugHooks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugHooks('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugHooks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRepositoriesUsernameRepoSlugHooks - errors', () => {
      it('should have a postRepositoriesUsernameRepoSlugHooks function', (done) => {
        try {
          assert.equal(true, typeof a.postRepositoriesUsernameRepoSlugHooks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugHooks(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postRepositoriesUsernameRepoSlugHooks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugHooks('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postRepositoriesUsernameRepoSlugHooks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRepositoriesUsernameRepoSlugHooksUid - errors', () => {
      it('should have a deleteRepositoriesUsernameRepoSlugHooksUid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRepositoriesUsernameRepoSlugHooksUid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.deleteRepositoriesUsernameRepoSlugHooksUid(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteRepositoriesUsernameRepoSlugHooksUid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.deleteRepositoriesUsernameRepoSlugHooksUid('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteRepositoriesUsernameRepoSlugHooksUid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uid', (done) => {
        try {
          a.deleteRepositoriesUsernameRepoSlugHooksUid('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'uid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteRepositoriesUsernameRepoSlugHooksUid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugHooksUid - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugHooksUid function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugHooksUid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugHooksUid(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugHooksUid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugHooksUid('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugHooksUid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uid', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugHooksUid('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'uid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugHooksUid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#CreateACommitByUploadingAFile ', () => {
      let un = 'testUsername';
      let repoSlug = 'testRepo';
      const message = 'Commmit message';
      const author = '';
      const parents = '';
      const files = { 'file1.txt': 'contents of file 1', 'testfoler/file2.txt': 'contents of file 2' };
      const branch = '';
      it('should have a CreateACommitByUploadingAFile function', (done) => {
        try {
          assert.equal(true, typeof a.CreateACommitByUploadingAFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('Should run successfully', (done) => {
        try {
          a.CreateACommitByUploadingAFile(un, repoSlug, message, author, parents, files, branch, (data, error) => {
            try {
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('Should fail with out username', (done) => {
        try {
          un = '';
          a.CreateACommitByUploadingAFile(un, repoSlug, message, author, parents, files, branch, (data, error) => {
            if (error.IAPerror.displayString === 'username is required') {
              done();
            } else {
              throw JSON.stringify(error);
            }
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('Should fail with out repoSlug', (done) => {
        try {
          un = 'testUsername';
          repoSlug = '';
          a.CreateACommitByUploadingAFile(un, repoSlug, message, author, parents, files, branch, (data, error) => {
            if (error.IAPerror.displayString === 'repoSlug is required') {
              done();
            } else {
              throw JSON.stringify(error);
            }
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRepositoriesUsernameRepoSlugHooksUid - errors', () => {
      it('should have a putRepositoriesUsernameRepoSlugHooksUid function', (done) => {
        try {
          assert.equal(true, typeof a.putRepositoriesUsernameRepoSlugHooksUid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.putRepositoriesUsernameRepoSlugHooksUid(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-putRepositoriesUsernameRepoSlugHooksUid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.putRepositoriesUsernameRepoSlugHooksUid('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-putRepositoriesUsernameRepoSlugHooksUid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uid', (done) => {
        try {
          a.putRepositoriesUsernameRepoSlugHooksUid('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'uid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-putRepositoriesUsernameRepoSlugHooksUid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositories - errors', () => {
      it('should have a getRepositories function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositories === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsername - errors', () => {
      it('should have a getRepositoriesUsername function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsername === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsername(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsername', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRepositoriesUsernameRepoSlug - errors', () => {
      it('should have a deleteRepositoriesUsernameRepoSlug function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRepositoriesUsernameRepoSlug === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.deleteRepositoriesUsernameRepoSlug(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteRepositoriesUsernameRepoSlug', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.deleteRepositoriesUsernameRepoSlug('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteRepositoriesUsernameRepoSlug', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlug - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlug function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlug === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlug(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlug', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlug('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlug', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRepositoriesUsernameRepoSlug - errors', () => {
      it('should have a postRepositoriesUsernameRepoSlug function', (done) => {
        try {
          assert.equal(true, typeof a.postRepositoriesUsernameRepoSlug === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlug(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postRepositoriesUsernameRepoSlug', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlug('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postRepositoriesUsernameRepoSlug', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRepositoriesUsernameRepoSlug - errors', () => {
      it('should have a putRepositoriesUsernameRepoSlug function', (done) => {
        try {
          assert.equal(true, typeof a.putRepositoriesUsernameRepoSlug === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.putRepositoriesUsernameRepoSlug(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-putRepositoriesUsernameRepoSlug', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.putRepositoriesUsernameRepoSlug('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-putRepositoriesUsernameRepoSlug', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugCommitNodeStatuses - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugCommitNodeStatuses function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugCommitNodeStatuses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugCommitNodeStatuses(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugCommitNodeStatuses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugCommitNodeStatuses('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugCommitNodeStatuses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing node', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugCommitNodeStatuses('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'node is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugCommitNodeStatuses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRepositoriesUsernameRepoSlugCommitNodeStatusesBuild - errors', () => {
      it('should have a postRepositoriesUsernameRepoSlugCommitNodeStatusesBuild function', (done) => {
        try {
          assert.equal(true, typeof a.postRepositoriesUsernameRepoSlugCommitNodeStatusesBuild === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugCommitNodeStatusesBuild(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postRepositoriesUsernameRepoSlugCommitNodeStatusesBuild', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugCommitNodeStatusesBuild('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postRepositoriesUsernameRepoSlugCommitNodeStatusesBuild', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing node', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugCommitNodeStatusesBuild('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'node is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postRepositoriesUsernameRepoSlugCommitNodeStatusesBuild', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugCommitNodeStatusesBuildKey - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugCommitNodeStatusesBuildKey function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugCommitNodeStatusesBuildKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugCommitNodeStatusesBuildKey(null, null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugCommitNodeStatusesBuildKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugCommitNodeStatusesBuildKey('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugCommitNodeStatusesBuildKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing node', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugCommitNodeStatusesBuildKey('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'node is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugCommitNodeStatusesBuildKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugCommitNodeStatusesBuildKey('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugCommitNodeStatusesBuildKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRepositoriesUsernameRepoSlugCommitNodeStatusesBuildKey - errors', () => {
      it('should have a putRepositoriesUsernameRepoSlugCommitNodeStatusesBuildKey function', (done) => {
        try {
          assert.equal(true, typeof a.putRepositoriesUsernameRepoSlugCommitNodeStatusesBuildKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.putRepositoriesUsernameRepoSlugCommitNodeStatusesBuildKey(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-putRepositoriesUsernameRepoSlugCommitNodeStatusesBuildKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.putRepositoriesUsernameRepoSlugCommitNodeStatusesBuildKey('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-putRepositoriesUsernameRepoSlugCommitNodeStatusesBuildKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing node', (done) => {
        try {
          a.putRepositoriesUsernameRepoSlugCommitNodeStatusesBuildKey('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'node is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-putRepositoriesUsernameRepoSlugCommitNodeStatusesBuildKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.putRepositoriesUsernameRepoSlugCommitNodeStatusesBuildKey('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-putRepositoriesUsernameRepoSlugCommitNodeStatusesBuildKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugForks - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugForks function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugForks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugForks(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugForks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugForks('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugForks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdStatuses - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdStatuses function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdStatuses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdStatuses(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdStatuses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdStatuses('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdStatuses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdStatuses('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdStatuses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugWatchers - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugWatchers function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugWatchers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugWatchers(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugWatchers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugWatchers('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugWatchers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTeamsUsernameHooks - errors', () => {
      it('should have a getTeamsUsernameHooks function', (done) => {
        try {
          assert.equal(true, typeof a.getTeamsUsernameHooks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getTeamsUsernameHooks(null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getTeamsUsernameHooks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTeamsUsernameHooks - errors', () => {
      it('should have a postTeamsUsernameHooks function', (done) => {
        try {
          assert.equal(true, typeof a.postTeamsUsernameHooks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.postTeamsUsernameHooks(null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postTeamsUsernameHooks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTeamsUsernameHooksUid - errors', () => {
      it('should have a deleteTeamsUsernameHooksUid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTeamsUsernameHooksUid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.deleteTeamsUsernameHooksUid(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteTeamsUsernameHooksUid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uid', (done) => {
        try {
          a.deleteTeamsUsernameHooksUid('fakeparam', null, (data, error) => {
            try {
              const displayE = 'uid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteTeamsUsernameHooksUid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTeamsUsernameHooksUid - errors', () => {
      it('should have a getTeamsUsernameHooksUid function', (done) => {
        try {
          assert.equal(true, typeof a.getTeamsUsernameHooksUid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getTeamsUsernameHooksUid(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getTeamsUsernameHooksUid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uid', (done) => {
        try {
          a.getTeamsUsernameHooksUid('fakeparam', null, (data, error) => {
            try {
              const displayE = 'uid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getTeamsUsernameHooksUid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putTeamsUsernameHooksUid - errors', () => {
      it('should have a putTeamsUsernameHooksUid function', (done) => {
        try {
          assert.equal(true, typeof a.putTeamsUsernameHooksUid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.putTeamsUsernameHooksUid(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-putTeamsUsernameHooksUid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uid', (done) => {
        try {
          a.putTeamsUsernameHooksUid('fakeparam', null, (data, error) => {
            try {
              const displayE = 'uid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-putTeamsUsernameHooksUid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTeams - errors', () => {
      it('should have a getTeams function', (done) => {
        try {
          assert.equal(true, typeof a.getTeams === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTeamsUsername - errors', () => {
      it('should have a getTeamsUsername function', (done) => {
        try {
          assert.equal(true, typeof a.getTeamsUsername === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getTeamsUsername(null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getTeamsUsername', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTeamsUsernameFollowers - errors', () => {
      it('should have a getTeamsUsernameFollowers function', (done) => {
        try {
          assert.equal(true, typeof a.getTeamsUsernameFollowers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getTeamsUsernameFollowers(null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getTeamsUsernameFollowers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTeamsUsernameFollowing - errors', () => {
      it('should have a getTeamsUsernameFollowing function', (done) => {
        try {
          assert.equal(true, typeof a.getTeamsUsernameFollowing === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getTeamsUsernameFollowing(null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getTeamsUsernameFollowing', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTeamsUsernameMembers - errors', () => {
      it('should have a getTeamsUsernameMembers function', (done) => {
        try {
          assert.equal(true, typeof a.getTeamsUsernameMembers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getTeamsUsernameMembers(null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getTeamsUsernameMembers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersUsernameHooks - errors', () => {
      it('should have a getUsersUsernameHooks function', (done) => {
        try {
          assert.equal(true, typeof a.getUsersUsernameHooks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getUsersUsernameHooks(null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getUsersUsernameHooks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postUsersUsernameHooks - errors', () => {
      it('should have a postUsersUsernameHooks function', (done) => {
        try {
          assert.equal(true, typeof a.postUsersUsernameHooks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.postUsersUsernameHooks(null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postUsersUsernameHooks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUsersUsernameHooksUid - errors', () => {
      it('should have a deleteUsersUsernameHooksUid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUsersUsernameHooksUid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.deleteUsersUsernameHooksUid(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteUsersUsernameHooksUid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uid', (done) => {
        try {
          a.deleteUsersUsernameHooksUid('fakeparam', null, (data, error) => {
            try {
              const displayE = 'uid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteUsersUsernameHooksUid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersUsernameHooksUid - errors', () => {
      it('should have a getUsersUsernameHooksUid function', (done) => {
        try {
          assert.equal(true, typeof a.getUsersUsernameHooksUid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getUsersUsernameHooksUid(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getUsersUsernameHooksUid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uid', (done) => {
        try {
          a.getUsersUsernameHooksUid('fakeparam', null, (data, error) => {
            try {
              const displayE = 'uid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getUsersUsernameHooksUid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putUsersUsernameHooksUid - errors', () => {
      it('should have a putUsersUsernameHooksUid function', (done) => {
        try {
          assert.equal(true, typeof a.putUsersUsernameHooksUid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.putUsersUsernameHooksUid(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-putUsersUsernameHooksUid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uid', (done) => {
        try {
          a.putUsersUsernameHooksUid('fakeparam', null, (data, error) => {
            try {
              const displayE = 'uid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-putUsersUsernameHooksUid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTeamsUsernameRepositories - errors', () => {
      it('should have a getTeamsUsernameRepositories function', (done) => {
        try {
          assert.equal(true, typeof a.getTeamsUsernameRepositories === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getTeamsUsernameRepositories(null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getTeamsUsernameRepositories', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersUsernameRepositories - errors', () => {
      it('should have a getUsersUsernameRepositories function', (done) => {
        try {
          assert.equal(true, typeof a.getUsersUsernameRepositories === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getUsersUsernameRepositories(null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getUsersUsernameRepositories', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUser - errors', () => {
      it('should have a getUser function', (done) => {
        try {
          assert.equal(true, typeof a.getUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserEmails - errors', () => {
      it('should have a getUserEmails function', (done) => {
        try {
          assert.equal(true, typeof a.getUserEmails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserEmailsEmail - errors', () => {
      it('should have a getUserEmailsEmail function', (done) => {
        try {
          assert.equal(true, typeof a.getUserEmailsEmail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing email', (done) => {
        try {
          a.getUserEmailsEmail(null, (data, error) => {
            try {
              const displayE = 'email is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getUserEmailsEmail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersUsername - errors', () => {
      it('should have a getUsersUsername function', (done) => {
        try {
          assert.equal(true, typeof a.getUsersUsername === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getUsersUsername(null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getUsersUsername', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersUsernameFollowers - errors', () => {
      it('should have a getUsersUsernameFollowers function', (done) => {
        try {
          assert.equal(true, typeof a.getUsersUsernameFollowers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getUsersUsernameFollowers(null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getUsersUsernameFollowers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersUsernameFollowing - errors', () => {
      it('should have a getUsersUsernameFollowing function', (done) => {
        try {
          assert.equal(true, typeof a.getUsersUsernameFollowing === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getUsersUsernameFollowing(null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getUsersUsernameFollowing', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugSrcNodePath - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugSrcNodePath function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugSrcNodePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugSrcNodePath(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugSrcNodePath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing node', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugSrcNodePath('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'node is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugSrcNodePath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugSrcNodePath('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugSrcNodePath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugSrcNodePath('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugSrcNodePath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugBranchRestrictions - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugBranchRestrictions function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugBranchRestrictions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugBranchRestrictions(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugBranchRestrictions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugBranchRestrictions('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugBranchRestrictions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRepositoriesUsernameRepoSlugBranchRestrictions - errors', () => {
      it('should have a postRepositoriesUsernameRepoSlugBranchRestrictions function', (done) => {
        try {
          assert.equal(true, typeof a.postRepositoriesUsernameRepoSlugBranchRestrictions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugBranchRestrictions(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postRepositoriesUsernameRepoSlugBranchRestrictions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugBranchRestrictions('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postRepositoriesUsernameRepoSlugBranchRestrictions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugBranchRestrictions('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postRepositoriesUsernameRepoSlugBranchRestrictions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRepositoriesUsernameRepoSlugBranchRestrictionsId - errors', () => {
      it('should have a deleteRepositoriesUsernameRepoSlugBranchRestrictionsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRepositoriesUsernameRepoSlugBranchRestrictionsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.deleteRepositoriesUsernameRepoSlugBranchRestrictionsId(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteRepositoriesUsernameRepoSlugBranchRestrictionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.deleteRepositoriesUsernameRepoSlugBranchRestrictionsId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteRepositoriesUsernameRepoSlugBranchRestrictionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteRepositoriesUsernameRepoSlugBranchRestrictionsId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteRepositoriesUsernameRepoSlugBranchRestrictionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugBranchRestrictionsId - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugBranchRestrictionsId function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugBranchRestrictionsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugBranchRestrictionsId(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugBranchRestrictionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugBranchRestrictionsId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugBranchRestrictionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugBranchRestrictionsId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugBranchRestrictionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRepositoriesUsernameRepoSlugBranchRestrictionsId - errors', () => {
      it('should have a putRepositoriesUsernameRepoSlugBranchRestrictionsId function', (done) => {
        try {
          assert.equal(true, typeof a.putRepositoriesUsernameRepoSlugBranchRestrictionsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.putRepositoriesUsernameRepoSlugBranchRestrictionsId(null, null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-putRepositoriesUsernameRepoSlugBranchRestrictionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.putRepositoriesUsernameRepoSlugBranchRestrictionsId('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-putRepositoriesUsernameRepoSlugBranchRestrictionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putRepositoriesUsernameRepoSlugBranchRestrictionsId('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-putRepositoriesUsernameRepoSlugBranchRestrictionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putRepositoriesUsernameRepoSlugBranchRestrictionsId('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-putRepositoriesUsernameRepoSlugBranchRestrictionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRepositoriesUsernameRepoSlugCommitNodeApprove - errors', () => {
      it('should have a deleteRepositoriesUsernameRepoSlugCommitNodeApprove function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRepositoriesUsernameRepoSlugCommitNodeApprove === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.deleteRepositoriesUsernameRepoSlugCommitNodeApprove(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteRepositoriesUsernameRepoSlugCommitNodeApprove', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.deleteRepositoriesUsernameRepoSlugCommitNodeApprove('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteRepositoriesUsernameRepoSlugCommitNodeApprove', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing node', (done) => {
        try {
          a.deleteRepositoriesUsernameRepoSlugCommitNodeApprove('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'node is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteRepositoriesUsernameRepoSlugCommitNodeApprove', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRepositoriesUsernameRepoSlugCommitNodeApprove - errors', () => {
      it('should have a postRepositoriesUsernameRepoSlugCommitNodeApprove function', (done) => {
        try {
          assert.equal(true, typeof a.postRepositoriesUsernameRepoSlugCommitNodeApprove === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugCommitNodeApprove(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postRepositoriesUsernameRepoSlugCommitNodeApprove', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugCommitNodeApprove('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postRepositoriesUsernameRepoSlugCommitNodeApprove', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing node', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugCommitNodeApprove('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'node is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postRepositoriesUsernameRepoSlugCommitNodeApprove', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugCommitRevision - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugCommitRevision function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugCommitRevision === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugCommitRevision(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugCommitRevision', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugCommitRevision('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugCommitRevision', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing revision', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugCommitRevision('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'revision is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugCommitRevision', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugCommitShaComments - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugCommitShaComments function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugCommitShaComments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugCommitShaComments(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugCommitShaComments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sha', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugCommitShaComments('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sha is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugCommitShaComments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugCommitShaComments('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugCommitShaComments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugCommitShaCommentsCommentId - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugCommitShaCommentsCommentId function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugCommitShaCommentsCommentId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugCommitShaCommentsCommentId(null, null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugCommitShaCommentsCommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sha', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugCommitShaCommentsCommentId('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'sha is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugCommitShaCommentsCommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commentId', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugCommitShaCommentsCommentId('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'commentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugCommitShaCommentsCommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugCommitShaCommentsCommentId('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugCommitShaCommentsCommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugCommits - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugCommits function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugCommits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugCommits(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugCommits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugCommits('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugCommits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRepositoriesUsernameRepoSlugCommits - errors', () => {
      it('should have a postRepositoriesUsernameRepoSlugCommits function', (done) => {
        try {
          assert.equal(true, typeof a.postRepositoriesUsernameRepoSlugCommits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugCommits(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postRepositoriesUsernameRepoSlugCommits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugCommits('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postRepositoriesUsernameRepoSlugCommits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugCommitsRevision - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugCommitsRevision function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugCommitsRevision === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugCommitsRevision(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugCommitsRevision', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing revision', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugCommitsRevision('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'revision is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugCommitsRevision', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugCommitsRevision('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugCommitsRevision', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRepositoriesUsernameRepoSlugCommitsRevision - errors', () => {
      it('should have a postRepositoriesUsernameRepoSlugCommitsRevision function', (done) => {
        try {
          assert.equal(true, typeof a.postRepositoriesUsernameRepoSlugCommitsRevision === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugCommitsRevision(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postRepositoriesUsernameRepoSlugCommitsRevision', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing revision', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugCommitsRevision('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'revision is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postRepositoriesUsernameRepoSlugCommitsRevision', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugCommitsRevision('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postRepositoriesUsernameRepoSlugCommitsRevision', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugDiffSpec - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugDiffSpec function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugDiffSpec === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugDiffSpec(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugDiffSpec', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing spec', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugDiffSpec('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'spec is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugDiffSpec', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugDiffSpec('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugDiffSpec', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugPatchSpec - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugPatchSpec function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugPatchSpec === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugPatchSpec(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugPatchSpec', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing spec', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugPatchSpec('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'spec is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugPatchSpec', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugPatchSpec('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugPatchSpec', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugComponents - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugComponents function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugComponents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugComponents(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugComponents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugComponents('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugComponents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugComponentsComponentId - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugComponentsComponentId function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugComponentsComponentId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugComponentsComponentId(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugComponentsComponentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugComponentsComponentId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugComponentsComponentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing componentId', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugComponentsComponentId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'componentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugComponentsComponentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugIssues - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugIssues function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugIssues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugIssues(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugIssues', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugIssues('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugIssues', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRepositoriesUsernameRepoSlugIssues - errors', () => {
      it('should have a postRepositoriesUsernameRepoSlugIssues function', (done) => {
        try {
          assert.equal(true, typeof a.postRepositoriesUsernameRepoSlugIssues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugIssues(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postRepositoriesUsernameRepoSlugIssues', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugIssues('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postRepositoriesUsernameRepoSlugIssues', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugIssues('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postRepositoriesUsernameRepoSlugIssues', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRepositoriesUsernameRepoSlugIssuesIssueId - errors', () => {
      it('should have a deleteRepositoriesUsernameRepoSlugIssuesIssueId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRepositoriesUsernameRepoSlugIssuesIssueId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.deleteRepositoriesUsernameRepoSlugIssuesIssueId(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteRepositoriesUsernameRepoSlugIssuesIssueId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueId', (done) => {
        try {
          a.deleteRepositoriesUsernameRepoSlugIssuesIssueId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'issueId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteRepositoriesUsernameRepoSlugIssuesIssueId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.deleteRepositoriesUsernameRepoSlugIssuesIssueId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteRepositoriesUsernameRepoSlugIssuesIssueId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugIssuesIssueId - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugIssuesIssueId function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugIssuesIssueId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugIssuesIssueId(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugIssuesIssueId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueId', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugIssuesIssueId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'issueId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugIssuesIssueId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugIssuesIssueId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugIssuesIssueId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugIssuesIssueIdAttachments - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugIssuesIssueIdAttachments function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugIssuesIssueIdAttachments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugIssuesIssueIdAttachments(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugIssuesIssueIdAttachments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugIssuesIssueIdAttachments('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugIssuesIssueIdAttachments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueId', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugIssuesIssueIdAttachments('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'issueId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugIssuesIssueIdAttachments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRepositoriesUsernameRepoSlugIssuesIssueIdAttachments - errors', () => {
      it('should have a postRepositoriesUsernameRepoSlugIssuesIssueIdAttachments function', (done) => {
        try {
          assert.equal(true, typeof a.postRepositoriesUsernameRepoSlugIssuesIssueIdAttachments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugIssuesIssueIdAttachments(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postRepositoriesUsernameRepoSlugIssuesIssueIdAttachments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugIssuesIssueIdAttachments('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postRepositoriesUsernameRepoSlugIssuesIssueIdAttachments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueId', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugIssuesIssueIdAttachments('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'issueId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postRepositoriesUsernameRepoSlugIssuesIssueIdAttachments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPath - errors', () => {
      it('should have a deleteRepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPath function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.deleteRepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPath(null, null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteRepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.deleteRepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPath('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteRepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueId', (done) => {
        try {
          a.deleteRepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPath('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'issueId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteRepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.deleteRepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPath('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteRepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPath - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPath function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPath(null, null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPath('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueId', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPath('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'issueId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPath('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugIssuesIssueIdComments - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugIssuesIssueIdComments function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugIssuesIssueIdComments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugIssuesIssueIdComments(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugIssuesIssueIdComments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueId', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugIssuesIssueIdComments('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'issueId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugIssuesIssueIdComments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugIssuesIssueIdComments('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugIssuesIssueIdComments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugIssuesIssueIdCommentsCommentId - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugIssuesIssueIdCommentsCommentId function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugIssuesIssueIdCommentsCommentId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugIssuesIssueIdCommentsCommentId(null, null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugIssuesIssueIdCommentsCommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commentId', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugIssuesIssueIdCommentsCommentId('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'commentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugIssuesIssueIdCommentsCommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueId', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugIssuesIssueIdCommentsCommentId('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'issueId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugIssuesIssueIdCommentsCommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugIssuesIssueIdCommentsCommentId('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugIssuesIssueIdCommentsCommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRepositoriesUsernameRepoSlugIssuesIssueIdVote - errors', () => {
      it('should have a deleteRepositoriesUsernameRepoSlugIssuesIssueIdVote function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRepositoriesUsernameRepoSlugIssuesIssueIdVote === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.deleteRepositoriesUsernameRepoSlugIssuesIssueIdVote(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteRepositoriesUsernameRepoSlugIssuesIssueIdVote', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.deleteRepositoriesUsernameRepoSlugIssuesIssueIdVote('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteRepositoriesUsernameRepoSlugIssuesIssueIdVote', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueId', (done) => {
        try {
          a.deleteRepositoriesUsernameRepoSlugIssuesIssueIdVote('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'issueId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteRepositoriesUsernameRepoSlugIssuesIssueIdVote', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugIssuesIssueIdVote - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugIssuesIssueIdVote function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugIssuesIssueIdVote === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugIssuesIssueIdVote(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugIssuesIssueIdVote', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugIssuesIssueIdVote('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugIssuesIssueIdVote', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueId', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugIssuesIssueIdVote('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'issueId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugIssuesIssueIdVote', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRepositoriesUsernameRepoSlugIssuesIssueIdVote - errors', () => {
      it('should have a putRepositoriesUsernameRepoSlugIssuesIssueIdVote function', (done) => {
        try {
          assert.equal(true, typeof a.putRepositoriesUsernameRepoSlugIssuesIssueIdVote === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.putRepositoriesUsernameRepoSlugIssuesIssueIdVote(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-putRepositoriesUsernameRepoSlugIssuesIssueIdVote', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.putRepositoriesUsernameRepoSlugIssuesIssueIdVote('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-putRepositoriesUsernameRepoSlugIssuesIssueIdVote', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueId', (done) => {
        try {
          a.putRepositoriesUsernameRepoSlugIssuesIssueIdVote('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'issueId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-putRepositoriesUsernameRepoSlugIssuesIssueIdVote', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRepositoriesUsernameRepoSlugIssuesIssueIdWatch - errors', () => {
      it('should have a deleteRepositoriesUsernameRepoSlugIssuesIssueIdWatch function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRepositoriesUsernameRepoSlugIssuesIssueIdWatch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.deleteRepositoriesUsernameRepoSlugIssuesIssueIdWatch(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteRepositoriesUsernameRepoSlugIssuesIssueIdWatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.deleteRepositoriesUsernameRepoSlugIssuesIssueIdWatch('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteRepositoriesUsernameRepoSlugIssuesIssueIdWatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueId', (done) => {
        try {
          a.deleteRepositoriesUsernameRepoSlugIssuesIssueIdWatch('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'issueId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteRepositoriesUsernameRepoSlugIssuesIssueIdWatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugIssuesIssueIdWatch - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugIssuesIssueIdWatch function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugIssuesIssueIdWatch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugIssuesIssueIdWatch(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugIssuesIssueIdWatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugIssuesIssueIdWatch('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugIssuesIssueIdWatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueId', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugIssuesIssueIdWatch('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'issueId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugIssuesIssueIdWatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRepositoriesUsernameRepoSlugIssuesIssueIdWatch - errors', () => {
      it('should have a putRepositoriesUsernameRepoSlugIssuesIssueIdWatch function', (done) => {
        try {
          assert.equal(true, typeof a.putRepositoriesUsernameRepoSlugIssuesIssueIdWatch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.putRepositoriesUsernameRepoSlugIssuesIssueIdWatch(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-putRepositoriesUsernameRepoSlugIssuesIssueIdWatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.putRepositoriesUsernameRepoSlugIssuesIssueIdWatch('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-putRepositoriesUsernameRepoSlugIssuesIssueIdWatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing issueId', (done) => {
        try {
          a.putRepositoriesUsernameRepoSlugIssuesIssueIdWatch('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'issueId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-putRepositoriesUsernameRepoSlugIssuesIssueIdWatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugMilestones - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugMilestones function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugMilestones === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugMilestones(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugMilestones', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugMilestones('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugMilestones', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugMilestonesMilestoneId - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugMilestonesMilestoneId function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugMilestonesMilestoneId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugMilestonesMilestoneId(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugMilestonesMilestoneId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugMilestonesMilestoneId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugMilestonesMilestoneId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing milestoneId', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugMilestonesMilestoneId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'milestoneId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugMilestonesMilestoneId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugVersions - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugVersions function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugVersions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugVersions(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugVersions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugVersions('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugVersions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugVersionsVersionId - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugVersionsVersionId function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugVersionsVersionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugVersionsVersionId(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugVersionsVersionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugVersionsVersionId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugVersionsVersionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing versionId', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugVersionsVersionId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'versionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugVersionsVersionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugDefaultReviewers - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugDefaultReviewers function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugDefaultReviewers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugDefaultReviewers(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugDefaultReviewers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugDefaultReviewers('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugDefaultReviewers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRepositoriesUsernameRepoSlugDefaultReviewersTargetUsername - errors', () => {
      it('should have a deleteRepositoriesUsernameRepoSlugDefaultReviewersTargetUsername function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRepositoriesUsernameRepoSlugDefaultReviewersTargetUsername === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.deleteRepositoriesUsernameRepoSlugDefaultReviewersTargetUsername(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteRepositoriesUsernameRepoSlugDefaultReviewersTargetUsername', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing targetUsername', (done) => {
        try {
          a.deleteRepositoriesUsernameRepoSlugDefaultReviewersTargetUsername('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'targetUsername is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteRepositoriesUsernameRepoSlugDefaultReviewersTargetUsername', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.deleteRepositoriesUsernameRepoSlugDefaultReviewersTargetUsername('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteRepositoriesUsernameRepoSlugDefaultReviewersTargetUsername', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugDefaultReviewersTargetUsername - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugDefaultReviewersTargetUsername function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugDefaultReviewersTargetUsername === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugDefaultReviewersTargetUsername(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugDefaultReviewersTargetUsername', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing targetUsername', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugDefaultReviewersTargetUsername('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'targetUsername is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugDefaultReviewersTargetUsername', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugDefaultReviewersTargetUsername('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugDefaultReviewersTargetUsername', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRepositoriesUsernameRepoSlugDefaultReviewersTargetUsername - errors', () => {
      it('should have a putRepositoriesUsernameRepoSlugDefaultReviewersTargetUsername function', (done) => {
        try {
          assert.equal(true, typeof a.putRepositoriesUsernameRepoSlugDefaultReviewersTargetUsername === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.putRepositoriesUsernameRepoSlugDefaultReviewersTargetUsername(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-putRepositoriesUsernameRepoSlugDefaultReviewersTargetUsername', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing targetUsername', (done) => {
        try {
          a.putRepositoriesUsernameRepoSlugDefaultReviewersTargetUsername('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'targetUsername is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-putRepositoriesUsernameRepoSlugDefaultReviewersTargetUsername', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.putRepositoriesUsernameRepoSlugDefaultReviewersTargetUsername('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-putRepositoriesUsernameRepoSlugDefaultReviewersTargetUsername', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugPullrequests - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugPullrequests function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugPullrequests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugPullrequests(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugPullrequests', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugPullrequests('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugPullrequests', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRepositoriesUsernameRepoSlugPullrequests - errors', () => {
      it('should have a postRepositoriesUsernameRepoSlugPullrequests function', (done) => {
        try {
          assert.equal(true, typeof a.postRepositoriesUsernameRepoSlugPullrequests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugPullrequests(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postRepositoriesUsernameRepoSlugPullrequests', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugPullrequests('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postRepositoriesUsernameRepoSlugPullrequests', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugPullrequestsActivity - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugPullrequestsActivity function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugPullrequestsActivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugPullrequestsActivity(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugPullrequestsActivity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugPullrequestsActivity('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugPullrequestsActivity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugPullrequestsPullRequestId - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugPullrequestsPullRequestId function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugPullrequestsPullRequestId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugPullrequestsPullRequestId(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugPullrequestsPullRequestId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugPullrequestsPullRequestId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugPullrequestsPullRequestId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugPullrequestsPullRequestId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugPullrequestsPullRequestId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRepositoriesUsernameRepoSlugPullrequestsPullRequestId - errors', () => {
      it('should have a putRepositoriesUsernameRepoSlugPullrequestsPullRequestId function', (done) => {
        try {
          assert.equal(true, typeof a.putRepositoriesUsernameRepoSlugPullrequestsPullRequestId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.putRepositoriesUsernameRepoSlugPullrequestsPullRequestId(null, null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-putRepositoriesUsernameRepoSlugPullrequestsPullRequestId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.putRepositoriesUsernameRepoSlugPullrequestsPullRequestId('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-putRepositoriesUsernameRepoSlugPullrequestsPullRequestId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.putRepositoriesUsernameRepoSlugPullrequestsPullRequestId('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-putRepositoriesUsernameRepoSlugPullrequestsPullRequestId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdActivity - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdActivity function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdActivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdActivity(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdActivity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdActivity('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdActivity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdActivity('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdActivity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRepositoriesUsernameRepoSlugPullrequestsPullRequestIdApprove - errors', () => {
      it('should have a deleteRepositoriesUsernameRepoSlugPullrequestsPullRequestIdApprove function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRepositoriesUsernameRepoSlugPullrequestsPullRequestIdApprove === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.deleteRepositoriesUsernameRepoSlugPullrequestsPullRequestIdApprove(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteRepositoriesUsernameRepoSlugPullrequestsPullRequestIdApprove', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.deleteRepositoriesUsernameRepoSlugPullrequestsPullRequestIdApprove('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteRepositoriesUsernameRepoSlugPullrequestsPullRequestIdApprove', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.deleteRepositoriesUsernameRepoSlugPullrequestsPullRequestIdApprove('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteRepositoriesUsernameRepoSlugPullrequestsPullRequestIdApprove', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRepositoriesUsernameRepoSlugPullrequestsPullRequestIdApprove - errors', () => {
      it('should have a postRepositoriesUsernameRepoSlugPullrequestsPullRequestIdApprove function', (done) => {
        try {
          assert.equal(true, typeof a.postRepositoriesUsernameRepoSlugPullrequestsPullRequestIdApprove === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugPullrequestsPullRequestIdApprove(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postRepositoriesUsernameRepoSlugPullrequestsPullRequestIdApprove', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugPullrequestsPullRequestIdApprove('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postRepositoriesUsernameRepoSlugPullrequestsPullRequestIdApprove', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugPullrequestsPullRequestIdApprove('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postRepositoriesUsernameRepoSlugPullrequestsPullRequestIdApprove', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdComments - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdComments function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdComments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdComments(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdComments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdComments('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdComments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdComments('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdComments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommentsCommentId - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommentsCommentId function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommentsCommentId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommentsCommentId(null, null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommentsCommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommentsCommentId('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommentsCommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commentId', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommentsCommentId('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'commentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommentsCommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommentsCommentId('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommentsCommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommits - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommits function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommits(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommits('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommits('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRepositoriesUsernameRepoSlugPullrequestsPullRequestIdDecline - errors', () => {
      it('should have a postRepositoriesUsernameRepoSlugPullrequestsPullRequestIdDecline function', (done) => {
        try {
          assert.equal(true, typeof a.postRepositoriesUsernameRepoSlugPullrequestsPullRequestIdDecline === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugPullrequestsPullRequestIdDecline(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postRepositoriesUsernameRepoSlugPullrequestsPullRequestIdDecline', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugPullrequestsPullRequestIdDecline('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postRepositoriesUsernameRepoSlugPullrequestsPullRequestIdDecline', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugPullrequestsPullRequestIdDecline('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postRepositoriesUsernameRepoSlugPullrequestsPullRequestIdDecline', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdDiff - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdDiff function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdDiff === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdDiff(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdDiff', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdDiff('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdDiff', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdDiff('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdDiff', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRepositoriesUsernameRepoSlugPullrequestsPullRequestIdMerge - errors', () => {
      it('should have a postRepositoriesUsernameRepoSlugPullrequestsPullRequestIdMerge function', (done) => {
        try {
          assert.equal(true, typeof a.postRepositoriesUsernameRepoSlugPullrequestsPullRequestIdMerge === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugPullrequestsPullRequestIdMerge(null, null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postRepositoriesUsernameRepoSlugPullrequestsPullRequestIdMerge', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugPullrequestsPullRequestIdMerge('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postRepositoriesUsernameRepoSlugPullrequestsPullRequestIdMerge', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugPullrequestsPullRequestIdMerge('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postRepositoriesUsernameRepoSlugPullrequestsPullRequestIdMerge', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdPatch - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdPatch function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdPatch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdPatch(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdPatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdPatch('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdPatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdPatch('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdPatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugDownloads - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugDownloads function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugDownloads === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugDownloads(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugDownloads', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugDownloads('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugDownloads', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRepositoriesUsernameRepoSlugDownloads - errors', () => {
      it('should have a postRepositoriesUsernameRepoSlugDownloads function', (done) => {
        try {
          assert.equal(true, typeof a.postRepositoriesUsernameRepoSlugDownloads === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugDownloads(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postRepositoriesUsernameRepoSlugDownloads', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugDownloads('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postRepositoriesUsernameRepoSlugDownloads', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRepositoriesUsernameRepoSlugDownloadsFilename - errors', () => {
      it('should have a deleteRepositoriesUsernameRepoSlugDownloadsFilename function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRepositoriesUsernameRepoSlugDownloadsFilename === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.deleteRepositoriesUsernameRepoSlugDownloadsFilename(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteRepositoriesUsernameRepoSlugDownloadsFilename', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filename', (done) => {
        try {
          a.deleteRepositoriesUsernameRepoSlugDownloadsFilename('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'filename is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteRepositoriesUsernameRepoSlugDownloadsFilename', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.deleteRepositoriesUsernameRepoSlugDownloadsFilename('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteRepositoriesUsernameRepoSlugDownloadsFilename', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugDownloadsFilename - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugDownloadsFilename function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugDownloadsFilename === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugDownloadsFilename(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugDownloadsFilename', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filename', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugDownloadsFilename('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'filename is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugDownloadsFilename', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugDownloadsFilename('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugDownloadsFilename', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPipelinesForRepository - errors', () => {
      it('should have a getPipelinesForRepository function', (done) => {
        try {
          assert.equal(true, typeof a.getPipelinesForRepository === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getPipelinesForRepository(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getPipelinesForRepository', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getPipelinesForRepository('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getPipelinesForRepository', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createPipelineForRepository - errors', () => {
      it('should have a createPipelineForRepository function', (done) => {
        try {
          assert.equal(true, typeof a.createPipelineForRepository === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.createPipelineForRepository(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-createPipelineForRepository', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.createPipelineForRepository('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-createPipelineForRepository', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createPipelineForRepository('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-createPipelineForRepository', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPipelineForRepository - errors', () => {
      it('should have a getPipelineForRepository function', (done) => {
        try {
          assert.equal(true, typeof a.getPipelineForRepository === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getPipelineForRepository(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getPipelineForRepository', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getPipelineForRepository('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getPipelineForRepository', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pipelineUuid', (done) => {
        try {
          a.getPipelineForRepository('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'pipelineUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getPipelineForRepository', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPipelineStepsForRepository - errors', () => {
      it('should have a getPipelineStepsForRepository function', (done) => {
        try {
          assert.equal(true, typeof a.getPipelineStepsForRepository === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getPipelineStepsForRepository(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getPipelineStepsForRepository', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getPipelineStepsForRepository('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getPipelineStepsForRepository', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pipelineUuid', (done) => {
        try {
          a.getPipelineStepsForRepository('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'pipelineUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getPipelineStepsForRepository', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPipelineStepForRepository - errors', () => {
      it('should have a getPipelineStepForRepository function', (done) => {
        try {
          assert.equal(true, typeof a.getPipelineStepForRepository === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getPipelineStepForRepository(null, null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getPipelineStepForRepository', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getPipelineStepForRepository('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getPipelineStepForRepository', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pipelineUuid', (done) => {
        try {
          a.getPipelineStepForRepository('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'pipelineUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getPipelineStepForRepository', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing stepUuid', (done) => {
        try {
          a.getPipelineStepForRepository('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'stepUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getPipelineStepForRepository', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPipelineStepLogForRepository - errors', () => {
      it('should have a getPipelineStepLogForRepository function', (done) => {
        try {
          assert.equal(true, typeof a.getPipelineStepLogForRepository === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getPipelineStepLogForRepository(null, null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getPipelineStepLogForRepository', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getPipelineStepLogForRepository('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getPipelineStepLogForRepository', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pipelineUuid', (done) => {
        try {
          a.getPipelineStepLogForRepository('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'pipelineUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getPipelineStepLogForRepository', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing stepUuid', (done) => {
        try {
          a.getPipelineStepLogForRepository('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'stepUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getPipelineStepLogForRepository', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#stopPipeline - errors', () => {
      it('should have a stopPipeline function', (done) => {
        try {
          assert.equal(true, typeof a.stopPipeline === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.stopPipeline(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-stopPipeline', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.stopPipeline('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-stopPipeline', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pipelineUuid', (done) => {
        try {
          a.stopPipeline('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'pipelineUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-stopPipeline', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoryPipelineConfig - errors', () => {
      it('should have a getRepositoryPipelineConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoryPipelineConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoryPipelineConfig(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoryPipelineConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoryPipelineConfig('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoryPipelineConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateRepositoryPipelineConfig - errors', () => {
      it('should have a updateRepositoryPipelineConfig function', (done) => {
        try {
          assert.equal(true, typeof a.updateRepositoryPipelineConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.updateRepositoryPipelineConfig(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-updateRepositoryPipelineConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.updateRepositoryPipelineConfig('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-updateRepositoryPipelineConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateRepositoryPipelineConfig('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-updateRepositoryPipelineConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRepositoryPipelineKeyPair - errors', () => {
      it('should have a deleteRepositoryPipelineKeyPair function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRepositoryPipelineKeyPair === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.deleteRepositoryPipelineKeyPair(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteRepositoryPipelineKeyPair', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.deleteRepositoryPipelineKeyPair('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteRepositoryPipelineKeyPair', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoryPipelineSshKeyPair - errors', () => {
      it('should have a getRepositoryPipelineSshKeyPair function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoryPipelineSshKeyPair === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoryPipelineSshKeyPair(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoryPipelineSshKeyPair', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoryPipelineSshKeyPair('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoryPipelineSshKeyPair', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateRepositoryPipelineKeyPair - errors', () => {
      it('should have a updateRepositoryPipelineKeyPair function', (done) => {
        try {
          assert.equal(true, typeof a.updateRepositoryPipelineKeyPair === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.updateRepositoryPipelineKeyPair(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-updateRepositoryPipelineKeyPair', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.updateRepositoryPipelineKeyPair('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-updateRepositoryPipelineKeyPair', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateRepositoryPipelineKeyPair('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-updateRepositoryPipelineKeyPair', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoryPipelineKnownHosts - errors', () => {
      it('should have a getRepositoryPipelineKnownHosts function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoryPipelineKnownHosts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoryPipelineKnownHosts(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoryPipelineKnownHosts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoryPipelineKnownHosts('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoryPipelineKnownHosts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRepositoryPipelineKnownHost - errors', () => {
      it('should have a createRepositoryPipelineKnownHost function', (done) => {
        try {
          assert.equal(true, typeof a.createRepositoryPipelineKnownHost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.createRepositoryPipelineKnownHost(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-createRepositoryPipelineKnownHost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.createRepositoryPipelineKnownHost('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-createRepositoryPipelineKnownHost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createRepositoryPipelineKnownHost('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-createRepositoryPipelineKnownHost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRepositoryPipelineKnownHost - errors', () => {
      it('should have a deleteRepositoryPipelineKnownHost function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRepositoryPipelineKnownHost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.deleteRepositoryPipelineKnownHost(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteRepositoryPipelineKnownHost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.deleteRepositoryPipelineKnownHost('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteRepositoryPipelineKnownHost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing knownHostUuid', (done) => {
        try {
          a.deleteRepositoryPipelineKnownHost('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'knownHostUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteRepositoryPipelineKnownHost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoryPipelineKnownHost - errors', () => {
      it('should have a getRepositoryPipelineKnownHost function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoryPipelineKnownHost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoryPipelineKnownHost(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoryPipelineKnownHost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoryPipelineKnownHost('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoryPipelineKnownHost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing knownHostUuid', (done) => {
        try {
          a.getRepositoryPipelineKnownHost('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'knownHostUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoryPipelineKnownHost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateRepositoryPipelineKnownHost - errors', () => {
      it('should have a updateRepositoryPipelineKnownHost function', (done) => {
        try {
          assert.equal(true, typeof a.updateRepositoryPipelineKnownHost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.updateRepositoryPipelineKnownHost(null, null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-updateRepositoryPipelineKnownHost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.updateRepositoryPipelineKnownHost('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-updateRepositoryPipelineKnownHost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing knownHostUuid', (done) => {
        try {
          a.updateRepositoryPipelineKnownHost('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'knownHostUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-updateRepositoryPipelineKnownHost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateRepositoryPipelineKnownHost('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-updateRepositoryPipelineKnownHost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoryPipelineVariables - errors', () => {
      it('should have a getRepositoryPipelineVariables function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoryPipelineVariables === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoryPipelineVariables(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoryPipelineVariables', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoryPipelineVariables('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoryPipelineVariables', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRepositoryPipelineVariable - errors', () => {
      it('should have a createRepositoryPipelineVariable function', (done) => {
        try {
          assert.equal(true, typeof a.createRepositoryPipelineVariable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.createRepositoryPipelineVariable(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-createRepositoryPipelineVariable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.createRepositoryPipelineVariable('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-createRepositoryPipelineVariable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createRepositoryPipelineVariable('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-createRepositoryPipelineVariable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRepositoryPipelineVariable - errors', () => {
      it('should have a deleteRepositoryPipelineVariable function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRepositoryPipelineVariable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.deleteRepositoryPipelineVariable(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteRepositoryPipelineVariable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.deleteRepositoryPipelineVariable('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteRepositoryPipelineVariable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing variableUuid', (done) => {
        try {
          a.deleteRepositoryPipelineVariable('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'variableUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteRepositoryPipelineVariable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoryPipelineVariable - errors', () => {
      it('should have a getRepositoryPipelineVariable function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoryPipelineVariable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoryPipelineVariable(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoryPipelineVariable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoryPipelineVariable('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoryPipelineVariable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing variableUuid', (done) => {
        try {
          a.getRepositoryPipelineVariable('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'variableUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoryPipelineVariable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateRepositoryPipelineVariable - errors', () => {
      it('should have a updateRepositoryPipelineVariable function', (done) => {
        try {
          assert.equal(true, typeof a.updateRepositoryPipelineVariable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.updateRepositoryPipelineVariable(null, null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-updateRepositoryPipelineVariable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.updateRepositoryPipelineVariable('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-updateRepositoryPipelineVariable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing variableUuid', (done) => {
        try {
          a.updateRepositoryPipelineVariable('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'variableUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-updateRepositoryPipelineVariable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateRepositoryPipelineVariable('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-updateRepositoryPipelineVariable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPipelineVariablesForTeam - errors', () => {
      it('should have a getPipelineVariablesForTeam function', (done) => {
        try {
          assert.equal(true, typeof a.getPipelineVariablesForTeam === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getPipelineVariablesForTeam(null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getPipelineVariablesForTeam', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createPipelineVariableForTeam - errors', () => {
      it('should have a createPipelineVariableForTeam function', (done) => {
        try {
          assert.equal(true, typeof a.createPipelineVariableForTeam === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.createPipelineVariableForTeam(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-createPipelineVariableForTeam', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePipelineVariableForTeam - errors', () => {
      it('should have a deletePipelineVariableForTeam function', (done) => {
        try {
          assert.equal(true, typeof a.deletePipelineVariableForTeam === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.deletePipelineVariableForTeam(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deletePipelineVariableForTeam', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing variableUuid', (done) => {
        try {
          a.deletePipelineVariableForTeam('fakeparam', null, (data, error) => {
            try {
              const displayE = 'variableUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deletePipelineVariableForTeam', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPipelineVariableForTeam - errors', () => {
      it('should have a getPipelineVariableForTeam function', (done) => {
        try {
          assert.equal(true, typeof a.getPipelineVariableForTeam === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getPipelineVariableForTeam(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getPipelineVariableForTeam', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing variableUuid', (done) => {
        try {
          a.getPipelineVariableForTeam('fakeparam', null, (data, error) => {
            try {
              const displayE = 'variableUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getPipelineVariableForTeam', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePipelineVariableForTeam - errors', () => {
      it('should have a updatePipelineVariableForTeam function', (done) => {
        try {
          assert.equal(true, typeof a.updatePipelineVariableForTeam === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.updatePipelineVariableForTeam(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-updatePipelineVariableForTeam', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing variableUuid', (done) => {
        try {
          a.updatePipelineVariableForTeam('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'variableUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-updatePipelineVariableForTeam', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePipelineVariableForTeam('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-updatePipelineVariableForTeam', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPipelineVariablesForUser - errors', () => {
      it('should have a getPipelineVariablesForUser function', (done) => {
        try {
          assert.equal(true, typeof a.getPipelineVariablesForUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getPipelineVariablesForUser(null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getPipelineVariablesForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createPipelineVariableForUser - errors', () => {
      it('should have a createPipelineVariableForUser function', (done) => {
        try {
          assert.equal(true, typeof a.createPipelineVariableForUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.createPipelineVariableForUser(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-createPipelineVariableForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePipelineVariableForUser - errors', () => {
      it('should have a deletePipelineVariableForUser function', (done) => {
        try {
          assert.equal(true, typeof a.deletePipelineVariableForUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.deletePipelineVariableForUser(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deletePipelineVariableForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing variableUuid', (done) => {
        try {
          a.deletePipelineVariableForUser('fakeparam', null, (data, error) => {
            try {
              const displayE = 'variableUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deletePipelineVariableForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPipelineVariableForUser - errors', () => {
      it('should have a getPipelineVariableForUser function', (done) => {
        try {
          assert.equal(true, typeof a.getPipelineVariableForUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getPipelineVariableForUser(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getPipelineVariableForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing variableUuid', (done) => {
        try {
          a.getPipelineVariableForUser('fakeparam', null, (data, error) => {
            try {
              const displayE = 'variableUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getPipelineVariableForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePipelineVariableForUser - errors', () => {
      it('should have a updatePipelineVariableForUser function', (done) => {
        try {
          assert.equal(true, typeof a.updatePipelineVariableForUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.updatePipelineVariableForUser(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-updatePipelineVariableForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing variableUuid', (done) => {
        try {
          a.updatePipelineVariableForUser('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'variableUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-updatePipelineVariableForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePipelineVariableForUser('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-updatePipelineVariableForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugRefs - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugRefs function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugRefs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugRefs(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugRefs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugRefs('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugRefs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugRefsBranches - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugRefsBranches function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugRefsBranches === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugRefsBranches(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugRefsBranches', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugRefsBranches('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugRefsBranches', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugRefsBranchesName - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugRefsBranchesName function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugRefsBranchesName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugRefsBranchesName(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugRefsBranchesName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugRefsBranchesName('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugRefsBranchesName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugRefsBranchesName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugRefsBranchesName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugRefsTags - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugRefsTags function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugRefsTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugRefsTags(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugRefsTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugRefsTags('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugRefsTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRepositoriesUsernameRepoSlugRefsTags - errors', () => {
      it('should have a postRepositoriesUsernameRepoSlugRefsTags function', (done) => {
        try {
          assert.equal(true, typeof a.postRepositoriesUsernameRepoSlugRefsTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugRefsTags(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postRepositoriesUsernameRepoSlugRefsTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugRefsTags('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postRepositoriesUsernameRepoSlugRefsTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugRefsTags('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postRepositoriesUsernameRepoSlugRefsTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugRefsTagsName - errors', () => {
      it('should have a getRepositoriesUsernameRepoSlugRefsTagsName function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesUsernameRepoSlugRefsTagsName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugRefsTagsName(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugRefsTagsName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugRefsTagsName('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugRefsTagsName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repoSlug', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugRefsTagsName('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repoSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getRepositoriesUsernameRepoSlugRefsTagsName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSnippets - errors', () => {
      it('should have a getSnippets function', (done) => {
        try {
          assert.equal(true, typeof a.getSnippets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSnippets - errors', () => {
      it('should have a postSnippets function', (done) => {
        try {
          assert.equal(true, typeof a.postSnippets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postSnippets(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postSnippets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSnippetsUsername - errors', () => {
      it('should have a getSnippetsUsername function', (done) => {
        try {
          assert.equal(true, typeof a.getSnippetsUsername === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getSnippetsUsername('fakeparam', null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getSnippetsUsername', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSnippetsUsername - errors', () => {
      it('should have a postSnippetsUsername function', (done) => {
        try {
          assert.equal(true, typeof a.postSnippetsUsername === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.postSnippetsUsername(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postSnippetsUsername', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postSnippetsUsername('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postSnippetsUsername', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSnippetsUsernameEncodedId - errors', () => {
      it('should have a deleteSnippetsUsernameEncodedId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSnippetsUsernameEncodedId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.deleteSnippetsUsernameEncodedId(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteSnippetsUsernameEncodedId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing encodedId', (done) => {
        try {
          a.deleteSnippetsUsernameEncodedId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'encodedId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteSnippetsUsernameEncodedId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSnippetsUsernameEncodedId - errors', () => {
      it('should have a getSnippetsUsernameEncodedId function', (done) => {
        try {
          assert.equal(true, typeof a.getSnippetsUsernameEncodedId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getSnippetsUsernameEncodedId(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getSnippetsUsernameEncodedId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing encodedId', (done) => {
        try {
          a.getSnippetsUsernameEncodedId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'encodedId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getSnippetsUsernameEncodedId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putSnippetsUsernameEncodedId - errors', () => {
      it('should have a putSnippetsUsernameEncodedId function', (done) => {
        try {
          assert.equal(true, typeof a.putSnippetsUsernameEncodedId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.putSnippetsUsernameEncodedId(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-putSnippetsUsernameEncodedId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing encodedId', (done) => {
        try {
          a.putSnippetsUsernameEncodedId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'encodedId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-putSnippetsUsernameEncodedId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSnippetsUsernameEncodedIdComments - errors', () => {
      it('should have a getSnippetsUsernameEncodedIdComments function', (done) => {
        try {
          assert.equal(true, typeof a.getSnippetsUsernameEncodedIdComments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getSnippetsUsernameEncodedIdComments(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getSnippetsUsernameEncodedIdComments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing encodedId', (done) => {
        try {
          a.getSnippetsUsernameEncodedIdComments('fakeparam', null, (data, error) => {
            try {
              const displayE = 'encodedId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getSnippetsUsernameEncodedIdComments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSnippetsUsernameEncodedIdComments - errors', () => {
      it('should have a postSnippetsUsernameEncodedIdComments function', (done) => {
        try {
          assert.equal(true, typeof a.postSnippetsUsernameEncodedIdComments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.postSnippetsUsernameEncodedIdComments(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postSnippetsUsernameEncodedIdComments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing encodedId', (done) => {
        try {
          a.postSnippetsUsernameEncodedIdComments('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'encodedId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postSnippetsUsernameEncodedIdComments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postSnippetsUsernameEncodedIdComments('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postSnippetsUsernameEncodedIdComments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSnippetsUsernameEncodedIdCommentsCommentId - errors', () => {
      it('should have a deleteSnippetsUsernameEncodedIdCommentsCommentId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSnippetsUsernameEncodedIdCommentsCommentId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.deleteSnippetsUsernameEncodedIdCommentsCommentId(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteSnippetsUsernameEncodedIdCommentsCommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commentId', (done) => {
        try {
          a.deleteSnippetsUsernameEncodedIdCommentsCommentId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'commentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteSnippetsUsernameEncodedIdCommentsCommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing encodedId', (done) => {
        try {
          a.deleteSnippetsUsernameEncodedIdCommentsCommentId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'encodedId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteSnippetsUsernameEncodedIdCommentsCommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSnippetsUsernameEncodedIdCommentsCommentId - errors', () => {
      it('should have a getSnippetsUsernameEncodedIdCommentsCommentId function', (done) => {
        try {
          assert.equal(true, typeof a.getSnippetsUsernameEncodedIdCommentsCommentId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getSnippetsUsernameEncodedIdCommentsCommentId(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getSnippetsUsernameEncodedIdCommentsCommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commentId', (done) => {
        try {
          a.getSnippetsUsernameEncodedIdCommentsCommentId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'commentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getSnippetsUsernameEncodedIdCommentsCommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing encodedId', (done) => {
        try {
          a.getSnippetsUsernameEncodedIdCommentsCommentId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'encodedId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getSnippetsUsernameEncodedIdCommentsCommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putSnippetsUsernameEncodedIdCommentsCommentId - errors', () => {
      it('should have a putSnippetsUsernameEncodedIdCommentsCommentId function', (done) => {
        try {
          assert.equal(true, typeof a.putSnippetsUsernameEncodedIdCommentsCommentId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.putSnippetsUsernameEncodedIdCommentsCommentId(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-putSnippetsUsernameEncodedIdCommentsCommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commentId', (done) => {
        try {
          a.putSnippetsUsernameEncodedIdCommentsCommentId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'commentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-putSnippetsUsernameEncodedIdCommentsCommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing encodedId', (done) => {
        try {
          a.putSnippetsUsernameEncodedIdCommentsCommentId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'encodedId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-putSnippetsUsernameEncodedIdCommentsCommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSnippetsUsernameEncodedIdCommits - errors', () => {
      it('should have a getSnippetsUsernameEncodedIdCommits function', (done) => {
        try {
          assert.equal(true, typeof a.getSnippetsUsernameEncodedIdCommits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getSnippetsUsernameEncodedIdCommits(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getSnippetsUsernameEncodedIdCommits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing encodedId', (done) => {
        try {
          a.getSnippetsUsernameEncodedIdCommits('fakeparam', null, (data, error) => {
            try {
              const displayE = 'encodedId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getSnippetsUsernameEncodedIdCommits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSnippetsUsernameEncodedIdCommitsRevision - errors', () => {
      it('should have a getSnippetsUsernameEncodedIdCommitsRevision function', (done) => {
        try {
          assert.equal(true, typeof a.getSnippetsUsernameEncodedIdCommitsRevision === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getSnippetsUsernameEncodedIdCommitsRevision(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getSnippetsUsernameEncodedIdCommitsRevision', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing encodedId', (done) => {
        try {
          a.getSnippetsUsernameEncodedIdCommitsRevision('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'encodedId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getSnippetsUsernameEncodedIdCommitsRevision', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing revision', (done) => {
        try {
          a.getSnippetsUsernameEncodedIdCommitsRevision('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'revision is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getSnippetsUsernameEncodedIdCommitsRevision', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSnippetsUsernameEncodedIdWatch - errors', () => {
      it('should have a deleteSnippetsUsernameEncodedIdWatch function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSnippetsUsernameEncodedIdWatch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.deleteSnippetsUsernameEncodedIdWatch(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteSnippetsUsernameEncodedIdWatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing encodedId', (done) => {
        try {
          a.deleteSnippetsUsernameEncodedIdWatch('fakeparam', null, (data, error) => {
            try {
              const displayE = 'encodedId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteSnippetsUsernameEncodedIdWatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSnippetsUsernameEncodedIdWatch - errors', () => {
      it('should have a getSnippetsUsernameEncodedIdWatch function', (done) => {
        try {
          assert.equal(true, typeof a.getSnippetsUsernameEncodedIdWatch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getSnippetsUsernameEncodedIdWatch(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getSnippetsUsernameEncodedIdWatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing encodedId', (done) => {
        try {
          a.getSnippetsUsernameEncodedIdWatch('fakeparam', null, (data, error) => {
            try {
              const displayE = 'encodedId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getSnippetsUsernameEncodedIdWatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putSnippetsUsernameEncodedIdWatch - errors', () => {
      it('should have a putSnippetsUsernameEncodedIdWatch function', (done) => {
        try {
          assert.equal(true, typeof a.putSnippetsUsernameEncodedIdWatch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.putSnippetsUsernameEncodedIdWatch(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-putSnippetsUsernameEncodedIdWatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing encodedId', (done) => {
        try {
          a.putSnippetsUsernameEncodedIdWatch('fakeparam', null, (data, error) => {
            try {
              const displayE = 'encodedId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-putSnippetsUsernameEncodedIdWatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSnippetsUsernameEncodedIdWatchers - errors', () => {
      it('should have a getSnippetsUsernameEncodedIdWatchers function', (done) => {
        try {
          assert.equal(true, typeof a.getSnippetsUsernameEncodedIdWatchers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getSnippetsUsernameEncodedIdWatchers(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getSnippetsUsernameEncodedIdWatchers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing encodedId', (done) => {
        try {
          a.getSnippetsUsernameEncodedIdWatchers('fakeparam', null, (data, error) => {
            try {
              const displayE = 'encodedId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getSnippetsUsernameEncodedIdWatchers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSnippetsUsernameEncodedIdNodeId - errors', () => {
      it('should have a deleteSnippetsUsernameEncodedIdNodeId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSnippetsUsernameEncodedIdNodeId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.deleteSnippetsUsernameEncodedIdNodeId(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteSnippetsUsernameEncodedIdNodeId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.deleteSnippetsUsernameEncodedIdNodeId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteSnippetsUsernameEncodedIdNodeId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing encodedId', (done) => {
        try {
          a.deleteSnippetsUsernameEncodedIdNodeId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'encodedId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteSnippetsUsernameEncodedIdNodeId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSnippetsUsernameEncodedIdNodeId - errors', () => {
      it('should have a getSnippetsUsernameEncodedIdNodeId function', (done) => {
        try {
          assert.equal(true, typeof a.getSnippetsUsernameEncodedIdNodeId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getSnippetsUsernameEncodedIdNodeId(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getSnippetsUsernameEncodedIdNodeId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing encodedId', (done) => {
        try {
          a.getSnippetsUsernameEncodedIdNodeId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'encodedId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getSnippetsUsernameEncodedIdNodeId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.getSnippetsUsernameEncodedIdNodeId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getSnippetsUsernameEncodedIdNodeId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putSnippetsUsernameEncodedIdNodeId - errors', () => {
      it('should have a putSnippetsUsernameEncodedIdNodeId function', (done) => {
        try {
          assert.equal(true, typeof a.putSnippetsUsernameEncodedIdNodeId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.putSnippetsUsernameEncodedIdNodeId(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-putSnippetsUsernameEncodedIdNodeId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing encodedId', (done) => {
        try {
          a.putSnippetsUsernameEncodedIdNodeId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'encodedId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-putSnippetsUsernameEncodedIdNodeId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.putSnippetsUsernameEncodedIdNodeId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-putSnippetsUsernameEncodedIdNodeId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSnippetsUsernameEncodedIdNodeIdFilesPath - errors', () => {
      it('should have a getSnippetsUsernameEncodedIdNodeIdFilesPath function', (done) => {
        try {
          assert.equal(true, typeof a.getSnippetsUsernameEncodedIdNodeIdFilesPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getSnippetsUsernameEncodedIdNodeIdFilesPath(null, null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getSnippetsUsernameEncodedIdNodeIdFilesPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathParam', (done) => {
        try {
          a.getSnippetsUsernameEncodedIdNodeIdFilesPath('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'pathParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getSnippetsUsernameEncodedIdNodeIdFilesPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.getSnippetsUsernameEncodedIdNodeIdFilesPath('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getSnippetsUsernameEncodedIdNodeIdFilesPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing encodedId', (done) => {
        try {
          a.getSnippetsUsernameEncodedIdNodeIdFilesPath('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'encodedId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getSnippetsUsernameEncodedIdNodeIdFilesPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSnippetsUsernameEncodedIdRevisionDiff - errors', () => {
      it('should have a getSnippetsUsernameEncodedIdRevisionDiff function', (done) => {
        try {
          assert.equal(true, typeof a.getSnippetsUsernameEncodedIdRevisionDiff === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getSnippetsUsernameEncodedIdRevisionDiff(null, null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getSnippetsUsernameEncodedIdRevisionDiff', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing encodedId', (done) => {
        try {
          a.getSnippetsUsernameEncodedIdRevisionDiff('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'encodedId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getSnippetsUsernameEncodedIdRevisionDiff', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing revision', (done) => {
        try {
          a.getSnippetsUsernameEncodedIdRevisionDiff('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'revision is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getSnippetsUsernameEncodedIdRevisionDiff', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSnippetsUsernameEncodedIdRevisionPatch - errors', () => {
      it('should have a getSnippetsUsernameEncodedIdRevisionPatch function', (done) => {
        try {
          assert.equal(true, typeof a.getSnippetsUsernameEncodedIdRevisionPatch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getSnippetsUsernameEncodedIdRevisionPatch(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getSnippetsUsernameEncodedIdRevisionPatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing encodedId', (done) => {
        try {
          a.getSnippetsUsernameEncodedIdRevisionPatch('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'encodedId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getSnippetsUsernameEncodedIdRevisionPatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing revision', (done) => {
        try {
          a.getSnippetsUsernameEncodedIdRevisionPatch('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'revision is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getSnippetsUsernameEncodedIdRevisionPatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTeamsOwnerProjects - errors', () => {
      it('should have a getTeamsOwnerProjects function', (done) => {
        try {
          assert.equal(true, typeof a.getTeamsOwnerProjects === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing owner', (done) => {
        try {
          a.getTeamsOwnerProjects(null, (data, error) => {
            try {
              const displayE = 'owner is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getTeamsOwnerProjects', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTeamsOwnerProjects - errors', () => {
      it('should have a postTeamsOwnerProjects function', (done) => {
        try {
          assert.equal(true, typeof a.postTeamsOwnerProjects === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing owner', (done) => {
        try {
          a.postTeamsOwnerProjects(null, null, (data, error) => {
            try {
              const displayE = 'owner is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postTeamsOwnerProjects', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postTeamsOwnerProjects('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-postTeamsOwnerProjects', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTeamsOwnerProjectsProjectKey - errors', () => {
      it('should have a deleteTeamsOwnerProjectsProjectKey function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTeamsOwnerProjectsProjectKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing owner', (done) => {
        try {
          a.deleteTeamsOwnerProjectsProjectKey(null, null, (data, error) => {
            try {
              const displayE = 'owner is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteTeamsOwnerProjectsProjectKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.deleteTeamsOwnerProjectsProjectKey('fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-deleteTeamsOwnerProjectsProjectKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTeamsOwnerProjectsProjectKey - errors', () => {
      it('should have a getTeamsOwnerProjectsProjectKey function', (done) => {
        try {
          assert.equal(true, typeof a.getTeamsOwnerProjectsProjectKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing owner', (done) => {
        try {
          a.getTeamsOwnerProjectsProjectKey(null, null, (data, error) => {
            try {
              const displayE = 'owner is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getTeamsOwnerProjectsProjectKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getTeamsOwnerProjectsProjectKey('fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-getTeamsOwnerProjectsProjectKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putTeamsOwnerProjectsProjectKey - errors', () => {
      it('should have a putTeamsOwnerProjectsProjectKey function', (done) => {
        try {
          assert.equal(true, typeof a.putTeamsOwnerProjectsProjectKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing owner', (done) => {
        try {
          a.putTeamsOwnerProjectsProjectKey(null, null, null, (data, error) => {
            try {
              const displayE = 'owner is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-putTeamsOwnerProjectsProjectKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.putTeamsOwnerProjectsProjectKey('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-putTeamsOwnerProjectsProjectKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putTeamsOwnerProjectsProjectKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket-adapter-putTeamsOwnerProjectsProjectKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
