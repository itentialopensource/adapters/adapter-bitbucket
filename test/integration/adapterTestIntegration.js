/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-bitbucket',
      type: 'Bitbucket',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const Bitbucket = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Bitbucket Adapter Test', () => {
  describe('Bitbucket Class Tests', () => {
    const a = new Bitbucket(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const addonLinkerKey = 'fakedata';
    describe('#postAddonLinkersLinkerKeyValues - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postAddonLinkersLinkerKeyValues(addonLinkerKey, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Addon', 'postAddonLinkersLinkerKeyValues', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putAddon - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putAddon((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Addon', 'putAddon', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAddonLinkers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAddonLinkers((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Addon', 'getAddonLinkers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAddonLinkersLinkerKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAddonLinkersLinkerKey(addonLinkerKey, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Addon', 'getAddonLinkersLinkerKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putAddonLinkersLinkerKeyValues - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putAddonLinkersLinkerKeyValues(addonLinkerKey, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Addon', 'putAddonLinkersLinkerKeyValues', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAddonLinkersLinkerKeyValues - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAddonLinkersLinkerKeyValues(addonLinkerKey, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Addon', 'getAddonLinkersLinkerKeyValues', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAddonLinkersLinkerKeyValues2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAddonLinkersLinkerKeyValues2(addonLinkerKey, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Addon', 'getAddonLinkersLinkerKeyValues2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHookEvents - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getHookEvents((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.repository);
                assert.equal('object', typeof data.response.team);
                assert.equal('object', typeof data.response.user);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webhooks', 'getHookEvents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webhooksSubjectType = 'fakedata';
    describe('#getHookEventsSubjectType - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getHookEventsSubjectType(webhooksSubjectType, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.next);
                assert.equal(9, data.response.page);
                assert.equal(3, data.response.pagelen);
                assert.equal('string', data.response.previous);
                assert.equal(2, data.response.size);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webhooks', 'getHookEventsSubjectType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const repositoriesUsername = 'fakedata';
    const repositoriesRepoSlug = 'fakedata';
    const repositoriesPostRepositoriesUsernameRepoSlugBodyParam = {};
    describe('#postRepositoriesUsernameRepoSlug - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlug(repositoriesUsername, repositoriesRepoSlug, repositoriesPostRepositoriesUsernameRepoSlugBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Repositories', 'postRepositoriesUsernameRepoSlug', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const repositoriesNode = 'fakedata';
    let repositoriesKey = 'fakedata';
    describe('#postRepositoriesUsernameRepoSlugCommitNodeStatusesBuild - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugCommitNodeStatusesBuild(repositoriesUsername, repositoriesRepoSlug, repositoriesNode, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.created_on);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.key);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.refname);
                assert.equal('FAILED', data.response.state);
                assert.equal('string', data.response.updated_on);
                assert.equal('string', data.response.url);
                assert.equal('string', data.response.uuid);
              } else {
                runCommonAsserts(data, error);
              }
              repositoriesKey = data.response.key;
              saveMockData('Repositories', 'postRepositoriesUsernameRepoSlugCommitNodeStatusesBuild', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRepositoriesUsernameRepoSlugHooks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugHooks(repositoriesUsername, repositoriesRepoSlug, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal(true, data.response.active);
                assert.equal('string', data.response.created_at);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.events));
                assert.equal('object', typeof data.response.subject);
                assert.equal('user', data.response.subject_type);
                assert.equal('string', data.response.url);
                assert.equal('string', data.response.uuid);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Repositories', 'postRepositoriesUsernameRepoSlugHooks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositories - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRepositories(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Repositories', 'getRepositories', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsername - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRepositoriesUsername(repositoriesUsername, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Repositories', 'getRepositoriesUsername', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const repositoriesPutRepositoriesUsernameRepoSlugBodyParam = {};
    describe('#putRepositoriesUsernameRepoSlug - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putRepositoriesUsernameRepoSlug(repositoriesUsername, repositoriesRepoSlug, repositoriesPutRepositoriesUsernameRepoSlugBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Repositories', 'putRepositoriesUsernameRepoSlug', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlug - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlug(repositoriesUsername, repositoriesRepoSlug, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Repositories', 'getRepositoriesUsernameRepoSlug', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugCommitNodeStatuses - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugCommitNodeStatuses(repositoriesUsername, repositoriesRepoSlug, repositoriesNode, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.next);
                assert.equal(5, data.response.page);
                assert.equal(9, data.response.pagelen);
                assert.equal('string', data.response.previous);
                assert.equal(4, data.response.size);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Repositories', 'getRepositoriesUsernameRepoSlugCommitNodeStatuses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const repositoriesPutRepositoriesUsernameRepoSlugCommitNodeStatusesBuildKeyBodyParam = {
      type: 'string',
      created_on: 'string',
      description: 'string',
      key: 'string',
      links: {
        commit: {
          href: 'string'
        },
        self: {
          href: 'string'
        }
      },
      name: 'string',
      refname: 'string',
      state: 'SUCCESSFUL',
      updated_on: 'string',
      url: 'string',
      uuid: 'string'
    };
    describe('#putRepositoriesUsernameRepoSlugCommitNodeStatusesBuildKey - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putRepositoriesUsernameRepoSlugCommitNodeStatusesBuildKey(repositoriesUsername, repositoriesRepoSlug, repositoriesNode, repositoriesKey, repositoriesPutRepositoriesUsernameRepoSlugCommitNodeStatusesBuildKeyBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.created_on);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.key);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.refname);
                assert.equal('SUCCESSFUL', data.response.state);
                assert.equal('string', data.response.updated_on);
                assert.equal('string', data.response.url);
                assert.equal('string', data.response.uuid);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Repositories', 'putRepositoriesUsernameRepoSlugCommitNodeStatusesBuildKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugCommitNodeStatusesBuildKey - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugCommitNodeStatusesBuildKey(repositoriesUsername, repositoriesRepoSlug, repositoriesNode, repositoriesKey, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.created_on);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.key);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.refname);
                assert.equal('INPROGRESS', data.response.state);
                assert.equal('string', data.response.updated_on);
                assert.equal('string', data.response.url);
                assert.equal('string', data.response.uuid);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Repositories', 'getRepositoriesUsernameRepoSlugCommitNodeStatusesBuildKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugForks - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugForks(repositoriesUsername, repositoriesRepoSlug, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Repositories', 'getRepositoriesUsernameRepoSlugForks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugHooks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugHooks(repositoriesUsername, repositoriesRepoSlug, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.next);
                assert.equal(6, data.response.page);
                assert.equal(10, data.response.pagelen);
                assert.equal('string', data.response.previous);
                assert.equal(4, data.response.size);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Repositories', 'getRepositoriesUsernameRepoSlugHooks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const repositoriesUid = 'fakedata';
    describe('#putRepositoriesUsernameRepoSlugHooksUid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putRepositoriesUsernameRepoSlugHooksUid(repositoriesUsername, repositoriesRepoSlug, repositoriesUid, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal(true, data.response.active);
                assert.equal('string', data.response.created_at);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.events));
                assert.equal('object', typeof data.response.subject);
                assert.equal('team', data.response.subject_type);
                assert.equal('string', data.response.url);
                assert.equal('string', data.response.uuid);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Repositories', 'putRepositoriesUsernameRepoSlugHooksUid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugHooksUid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugHooksUid(repositoriesUsername, repositoriesRepoSlug, repositoriesUid, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal(false, data.response.active);
                assert.equal('string', data.response.created_at);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.events));
                assert.equal('object', typeof data.response.subject);
                assert.equal('repository', data.response.subject_type);
                assert.equal('string', data.response.url);
                assert.equal('string', data.response.uuid);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Repositories', 'getRepositoriesUsernameRepoSlugHooksUid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const repositoriesPullRequestId = 555;
    describe('#getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdStatuses - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdStatuses(repositoriesUsername, repositoriesRepoSlug, repositoriesPullRequestId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.next);
                assert.equal(3, data.response.page);
                assert.equal(9, data.response.pagelen);
                assert.equal('string', data.response.previous);
                assert.equal(9, data.response.size);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Repositories', 'getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdStatuses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugWatchers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugWatchers(repositoriesUsername, repositoriesRepoSlug, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Repositories', 'getRepositoriesUsernameRepoSlugWatchers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const teamsUsername = 'fakedata';
    describe('#postTeamsUsernameHooks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTeamsUsernameHooks(teamsUsername, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal(false, data.response.active);
                assert.equal('string', data.response.created_at);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.events));
                assert.equal('object', typeof data.response.subject);
                assert.equal('team', data.response.subject_type);
                assert.equal('string', data.response.url);
                assert.equal('string', data.response.uuid);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'postTeamsUsernameHooks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTeams - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTeams(null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.next);
                assert.equal(3, data.response.page);
                assert.equal(4, data.response.pagelen);
                assert.equal('string', data.response.previous);
                assert.equal(5, data.response.size);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'getTeams', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTeamsUsername - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTeamsUsername(teamsUsername, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.created_on);
                assert.equal('string', data.response.display_name);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.username);
                assert.equal('string', data.response.uuid);
                assert.equal('string', data.response.website);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'getTeamsUsername', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTeamsUsernameFollowers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTeamsUsernameFollowers(teamsUsername, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.next);
                assert.equal(7, data.response.page);
                assert.equal(8, data.response.pagelen);
                assert.equal('string', data.response.previous);
                assert.equal(2, data.response.size);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'getTeamsUsernameFollowers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTeamsUsernameFollowing - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTeamsUsernameFollowing(teamsUsername, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.next);
                assert.equal(8, data.response.page);
                assert.equal(6, data.response.pagelen);
                assert.equal('string', data.response.previous);
                assert.equal(10, data.response.size);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'getTeamsUsernameFollowing', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTeamsUsernameHooks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTeamsUsernameHooks(teamsUsername, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.next);
                assert.equal(5, data.response.page);
                assert.equal(10, data.response.pagelen);
                assert.equal('string', data.response.previous);
                assert.equal(5, data.response.size);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'getTeamsUsernameHooks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const teamsUid = 'fakedata';
    describe('#putTeamsUsernameHooksUid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putTeamsUsernameHooksUid(teamsUsername, teamsUid, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal(true, data.response.active);
                assert.equal('string', data.response.created_at);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.events));
                assert.equal('object', typeof data.response.subject);
                assert.equal('user', data.response.subject_type);
                assert.equal('string', data.response.url);
                assert.equal('string', data.response.uuid);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'putTeamsUsernameHooksUid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTeamsUsernameHooksUid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTeamsUsernameHooksUid(teamsUsername, teamsUid, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal(false, data.response.active);
                assert.equal('string', data.response.created_at);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.events));
                assert.equal('object', typeof data.response.subject);
                assert.equal('repository', data.response.subject_type);
                assert.equal('string', data.response.url);
                assert.equal('string', data.response.uuid);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'getTeamsUsernameHooksUid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTeamsUsernameMembers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTeamsUsernameMembers(teamsUsername, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.created_on);
                assert.equal('string', data.response.display_name);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.username);
                assert.equal('string', data.response.uuid);
                assert.equal('string', data.response.website);
                assert.equal('string', data.response.account_id);
                assert.equal(false, data.response.is_staff);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'getTeamsUsernameMembers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersUsername = 'fakedata';
    describe('#postUsersUsernameHooks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postUsersUsernameHooks(usersUsername, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal(false, data.response.active);
                assert.equal('string', data.response.created_at);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.events));
                assert.equal('object', typeof data.response.subject);
                assert.equal('repository', data.response.subject_type);
                assert.equal('string', data.response.url);
                assert.equal('string', data.response.uuid);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'postUsersUsernameHooks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTeamsUsernameRepositories - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTeamsUsernameRepositories(usersUsername, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getTeamsUsernameRepositories', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUser((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.created_on);
                assert.equal('string', data.response.display_name);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.username);
                assert.equal('string', data.response.uuid);
                assert.equal('string', data.response.website);
                assert.equal('string', data.response.account_id);
                assert.equal(true, data.response.is_staff);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserEmails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getUserEmails((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getUserEmails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersEmail = 'fakedata';
    describe('#getUserEmailsEmail - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getUserEmailsEmail(usersEmail, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getUserEmailsEmail', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersUsername - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUsersUsername(usersUsername, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.created_on);
                assert.equal('string', data.response.display_name);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.username);
                assert.equal('string', data.response.uuid);
                assert.equal('string', data.response.website);
                assert.equal('string', data.response.account_id);
                assert.equal(true, data.response.is_staff);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getUsersUsername', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersUsernameFollowers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUsersUsernameFollowers(usersUsername, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.next);
                assert.equal(3, data.response.page);
                assert.equal(6, data.response.pagelen);
                assert.equal('string', data.response.previous);
                assert.equal(10, data.response.size);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getUsersUsernameFollowers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersUsernameFollowing - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUsersUsernameFollowing(usersUsername, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.next);
                assert.equal(10, data.response.page);
                assert.equal(1, data.response.pagelen);
                assert.equal('string', data.response.previous);
                assert.equal(3, data.response.size);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getUsersUsernameFollowing', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersUsernameHooks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUsersUsernameHooks(usersUsername, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.next);
                assert.equal(2, data.response.page);
                assert.equal(6, data.response.pagelen);
                assert.equal('string', data.response.previous);
                assert.equal(4, data.response.size);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getUsersUsernameHooks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersUid = 'fakedata';
    describe('#putUsersUsernameHooksUid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putUsersUsernameHooksUid(usersUsername, usersUid, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal(true, data.response.active);
                assert.equal('string', data.response.created_at);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.events));
                assert.equal('object', typeof data.response.subject);
                assert.equal('repository', data.response.subject_type);
                assert.equal('string', data.response.url);
                assert.equal('string', data.response.uuid);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'putUsersUsernameHooksUid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersUsernameHooksUid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUsersUsernameHooksUid(usersUsername, usersUid, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal(true, data.response.active);
                assert.equal('string', data.response.created_at);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.events));
                assert.equal('object', typeof data.response.subject);
                assert.equal('team', data.response.subject_type);
                assert.equal('string', data.response.url);
                assert.equal('string', data.response.uuid);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getUsersUsernameHooksUid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersUsernameRepositories - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getUsersUsernameRepositories(usersUsername, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getUsersUsernameRepositories', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sourceUsername = 'fakedata';
    const sourceNode = 'fakedata';
    const sourcePathParam = 'fakedata';
    const sourceRepoSlug = 'fakedata';
    describe('#getRepositoriesUsernameRepoSlugSrcNodePath - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugSrcNodePath(sourceUsername, sourceNode, sourcePathParam, sourceRepoSlug, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Source', 'getRepositoriesUsernameRepoSlugSrcNodePath', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const branchrestrictionsUsername = 'fakedata';
    const branchrestrictionsRepoSlug = 'fakedata';
    let branchrestrictionsId = 'fakedata';
    const branchrestrictionsPostRepositoriesUsernameRepoSlugBranchRestrictionsBodyParam = {
      type: 'string',
      groups: [
        {
          type: 'string',
          full_slug: 'string',
          links: {
            html: {
              href: 'string'
            },
            self: {
              href: 'string'
            }
          },
          members: 6,
          name: 'string',
          owner: {
            type: 'string',
            created_on: 'string',
            display_name: 'string',
            links: {
              avatar: {
                href: 'string'
              },
              followers: {
                href: 'string'
              },
              following: {
                href: 'string'
              },
              html: {
                href: 'string'
              },
              repositories: {
                href: 'string'
              },
              self: {
                href: 'string'
              }
            },
            username: 'string',
            uuid: 'string',
            website: 'string'
          },
          slug: 'string'
        }
      ],
      id: 1,
      kind: 'require_approvals_to_merge',
      links: {
        self: {
          href: 'string'
        }
      },
      users: [
        {
          type: 'string',
          created_on: 'string',
          display_name: 'string',
          links: {
            avatar: {
              href: 'string'
            },
            followers: {
              href: 'string'
            },
            following: {
              href: 'string'
            },
            html: {
              href: 'string'
            },
            repositories: {
              href: 'string'
            },
            self: {
              href: 'string'
            }
          },
          username: 'string',
          uuid: 'string',
          website: 'string'
        }
      ],
      value: 5
    };
    describe('#postRepositoriesUsernameRepoSlugBranchRestrictions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugBranchRestrictions(branchrestrictionsUsername, branchrestrictionsRepoSlug, branchrestrictionsPostRepositoriesUsernameRepoSlugBranchRestrictionsBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal(true, Array.isArray(data.response.groups));
                assert.equal(1, data.response.id);
                assert.equal('require_approvals_to_merge', data.response.kind);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.users));
                assert.equal(6, data.response.value);
              } else {
                runCommonAsserts(data, error);
              }
              branchrestrictionsId = data.response.id;
              saveMockData('Branchrestrictions', 'postRepositoriesUsernameRepoSlugBranchRestrictions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugBranchRestrictions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugBranchRestrictions(branchrestrictionsUsername, branchrestrictionsRepoSlug, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.next);
                assert.equal(5, data.response.page);
                assert.equal(7, data.response.pagelen);
                assert.equal('string', data.response.previous);
                assert.equal(9, data.response.size);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Branchrestrictions', 'getRepositoriesUsernameRepoSlugBranchRestrictions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const branchrestrictionsPutRepositoriesUsernameRepoSlugBranchRestrictionsIdBodyParam = {
      type: 'string',
      groups: [
        {
          type: 'string',
          full_slug: 'string',
          links: {
            html: {
              href: 'string'
            },
            self: {
              href: 'string'
            }
          },
          members: 10,
          name: 'string',
          owner: {
            type: 'string',
            created_on: 'string',
            display_name: 'string',
            links: {
              avatar: {
                href: 'string'
              },
              followers: {
                href: 'string'
              },
              following: {
                href: 'string'
              },
              html: {
                href: 'string'
              },
              repositories: {
                href: 'string'
              },
              self: {
                href: 'string'
              }
            },
            username: 'string',
            uuid: 'string',
            website: 'string'
          },
          slug: 'string'
        }
      ],
      id: 6,
      kind: 'require_approvals_to_merge',
      links: {
        self: {
          href: 'string'
        }
      },
      users: [
        {
          type: 'string',
          created_on: 'string',
          display_name: 'string',
          links: {
            avatar: {
              href: 'string'
            },
            followers: {
              href: 'string'
            },
            following: {
              href: 'string'
            },
            html: {
              href: 'string'
            },
            repositories: {
              href: 'string'
            },
            self: {
              href: 'string'
            }
          },
          username: 'string',
          uuid: 'string',
          website: 'string'
        }
      ],
      value: 10
    };
    describe('#putRepositoriesUsernameRepoSlugBranchRestrictionsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putRepositoriesUsernameRepoSlugBranchRestrictionsId(branchrestrictionsUsername, branchrestrictionsRepoSlug, branchrestrictionsId, branchrestrictionsPutRepositoriesUsernameRepoSlugBranchRestrictionsIdBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal(true, Array.isArray(data.response.groups));
                assert.equal(9, data.response.id);
                assert.equal('restrict_merges', data.response.kind);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.users));
                assert.equal(10, data.response.value);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Branchrestrictions', 'putRepositoriesUsernameRepoSlugBranchRestrictionsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugBranchRestrictionsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugBranchRestrictionsId(branchrestrictionsUsername, branchrestrictionsRepoSlug, branchrestrictionsId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal(true, Array.isArray(data.response.groups));
                assert.equal(8, data.response.id);
                assert.equal('push', data.response.kind);
                assert.equal('object', typeof data.response.links);
                assert.equal(true, Array.isArray(data.response.users));
                assert.equal(7, data.response.value);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Branchrestrictions', 'getRepositoriesUsernameRepoSlugBranchRestrictionsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const commitsUsername = 'fakedata';
    const commitsRepoSlug = 'fakedata';
    const commitsNode = 'fakedata';
    describe('#postRepositoriesUsernameRepoSlugCommitNodeApprove - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugCommitNodeApprove(commitsUsername, commitsRepoSlug, commitsNode, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal(true, data.response.approved);
                assert.equal('REVIEWER', data.response.role);
                assert.equal('object', typeof data.response.user);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Commits', 'postRepositoriesUsernameRepoSlugCommitNodeApprove', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRepositoriesUsernameRepoSlugCommits - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugCommits(commitsUsername, commitsRepoSlug, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Commits', 'postRepositoriesUsernameRepoSlugCommits', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const commitsRevision = 'fakedata';
    describe('#postRepositoriesUsernameRepoSlugCommitsRevision - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugCommitsRevision(commitsUsername, commitsRevision, commitsRepoSlug, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Commits', 'postRepositoriesUsernameRepoSlugCommitsRevision', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugCommitRevision - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugCommitRevision(commitsUsername, commitsRepoSlug, commitsRevision, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Commits', 'getRepositoriesUsernameRepoSlugCommitRevision', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const commitsSha = 'fakedata';
    describe('#getRepositoriesUsernameRepoSlugCommitShaComments - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugCommitShaComments(commitsUsername, commitsSha, commitsRepoSlug, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Commits', 'getRepositoriesUsernameRepoSlugCommitShaComments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const commitsCommentId = 'fakedata';
    describe('#getRepositoriesUsernameRepoSlugCommitShaCommentsCommentId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugCommitShaCommentsCommentId(commitsUsername, commitsSha, commitsCommentId, commitsRepoSlug, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Commits', 'getRepositoriesUsernameRepoSlugCommitShaCommentsCommentId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugCommits - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugCommits(commitsUsername, commitsRepoSlug, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Commits', 'getRepositoriesUsernameRepoSlugCommits', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugCommitsRevision - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugCommitsRevision(commitsUsername, commitsRevision, commitsRepoSlug, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Commits', 'getRepositoriesUsernameRepoSlugCommitsRevision', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const commitsSpec = 'fakedata';
    describe('#getRepositoriesUsernameRepoSlugDiffSpec - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugDiffSpec(commitsUsername, commitsSpec, commitsRepoSlug, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Commits', 'getRepositoriesUsernameRepoSlugDiffSpec', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugPatchSpec - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugPatchSpec(commitsUsername, commitsSpec, commitsRepoSlug, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Commits', 'getRepositoriesUsernameRepoSlugPatchSpec', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueTrackerUsername = 'fakedata';
    const issueTrackerRepoSlug = 'fakedata';
    const issueTrackerPostRepositoriesUsernameRepoSlugIssuesBodyParam = {};
    describe('#postRepositoriesUsernameRepoSlugIssues - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugIssues(issueTrackerUsername, issueTrackerRepoSlug, issueTrackerPostRepositoriesUsernameRepoSlugIssuesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueTracker', 'postRepositoriesUsernameRepoSlugIssues', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueTrackerIssueId = 555;
    describe('#postRepositoriesUsernameRepoSlugIssuesIssueIdAttachments - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugIssuesIssueIdAttachments(issueTrackerUsername, issueTrackerRepoSlug, issueTrackerIssueId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueTracker', 'postRepositoriesUsernameRepoSlugIssuesIssueIdAttachments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugComponents - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugComponents(issueTrackerUsername, issueTrackerRepoSlug, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.next);
                assert.equal(3, data.response.page);
                assert.equal(5, data.response.pagelen);
                assert.equal('string', data.response.previous);
                assert.equal(2, data.response.size);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueTracker', 'getRepositoriesUsernameRepoSlugComponents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueTrackerComponentId = 555;
    describe('#getRepositoriesUsernameRepoSlugComponentsComponentId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugComponentsComponentId(issueTrackerUsername, issueTrackerRepoSlug, issueTrackerComponentId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal(9, data.response.id);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueTracker', 'getRepositoriesUsernameRepoSlugComponentsComponentId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugIssues - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugIssues(issueTrackerUsername, issueTrackerRepoSlug, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueTracker', 'getRepositoriesUsernameRepoSlugIssues', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugIssuesIssueId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugIssuesIssueId(issueTrackerUsername, issueTrackerIssueId, issueTrackerRepoSlug, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueTracker', 'getRepositoriesUsernameRepoSlugIssuesIssueId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugIssuesIssueIdAttachments - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugIssuesIssueIdAttachments(issueTrackerUsername, issueTrackerRepoSlug, issueTrackerIssueId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.next);
                assert.equal(5, data.response.page);
                assert.equal(6, data.response.pagelen);
                assert.equal('string', data.response.previous);
                assert.equal(5, data.response.size);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueTracker', 'getRepositoriesUsernameRepoSlugIssuesIssueIdAttachments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueTrackerPathParam = 'fakedata';
    describe('#getRepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPath - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPath(issueTrackerUsername, issueTrackerPathParam, issueTrackerIssueId, issueTrackerRepoSlug, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueTracker', 'getRepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPath', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugIssuesIssueIdComments - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugIssuesIssueIdComments(issueTrackerUsername, issueTrackerIssueId, issueTrackerRepoSlug, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueTracker', 'getRepositoriesUsernameRepoSlugIssuesIssueIdComments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueTrackerCommentId = 'fakedata';
    describe('#getRepositoriesUsernameRepoSlugIssuesIssueIdCommentsCommentId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugIssuesIssueIdCommentsCommentId(issueTrackerUsername, issueTrackerCommentId, issueTrackerIssueId, issueTrackerRepoSlug, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueTracker', 'getRepositoriesUsernameRepoSlugIssuesIssueIdCommentsCommentId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRepositoriesUsernameRepoSlugIssuesIssueIdVote - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putRepositoriesUsernameRepoSlugIssuesIssueIdVote(issueTrackerUsername, issueTrackerRepoSlug, issueTrackerIssueId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.error);
                assert.equal('string', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueTracker', 'putRepositoriesUsernameRepoSlugIssuesIssueIdVote', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugIssuesIssueIdVote - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugIssuesIssueIdVote(issueTrackerUsername, issueTrackerRepoSlug, issueTrackerIssueId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.error);
                assert.equal('string', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueTracker', 'getRepositoriesUsernameRepoSlugIssuesIssueIdVote', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putRepositoriesUsernameRepoSlugIssuesIssueIdWatch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putRepositoriesUsernameRepoSlugIssuesIssueIdWatch(issueTrackerUsername, issueTrackerRepoSlug, issueTrackerIssueId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.error);
                assert.equal('string', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueTracker', 'putRepositoriesUsernameRepoSlugIssuesIssueIdWatch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugIssuesIssueIdWatch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugIssuesIssueIdWatch(issueTrackerUsername, issueTrackerRepoSlug, issueTrackerIssueId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.error);
                assert.equal('string', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueTracker', 'getRepositoriesUsernameRepoSlugIssuesIssueIdWatch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugMilestones - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugMilestones(issueTrackerUsername, issueTrackerRepoSlug, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.next);
                assert.equal(8, data.response.page);
                assert.equal(9, data.response.pagelen);
                assert.equal('string', data.response.previous);
                assert.equal(1, data.response.size);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueTracker', 'getRepositoriesUsernameRepoSlugMilestones', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueTrackerMilestoneId = 555;
    describe('#getRepositoriesUsernameRepoSlugMilestonesMilestoneId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugMilestonesMilestoneId(issueTrackerUsername, issueTrackerRepoSlug, issueTrackerMilestoneId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal(9, data.response.id);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueTracker', 'getRepositoriesUsernameRepoSlugMilestonesMilestoneId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugVersions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugVersions(issueTrackerUsername, issueTrackerRepoSlug, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.next);
                assert.equal(6, data.response.page);
                assert.equal(5, data.response.pagelen);
                assert.equal('string', data.response.previous);
                assert.equal(9, data.response.size);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueTracker', 'getRepositoriesUsernameRepoSlugVersions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const issueTrackerVersionId = 555;
    describe('#getRepositoriesUsernameRepoSlugVersionsVersionId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugVersionsVersionId(issueTrackerUsername, issueTrackerRepoSlug, issueTrackerVersionId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal(10, data.response.id);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueTracker', 'getRepositoriesUsernameRepoSlugVersionsVersionId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pullrequestsUsername = 'fakedata';
    const pullrequestsRepoSlug = 'fakedata';
    const pullrequestsPostRepositoriesUsernameRepoSlugPullrequestsBodyParam = {};
    describe('#postRepositoriesUsernameRepoSlugPullrequests - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugPullrequests(pullrequestsUsername, pullrequestsRepoSlug, pullrequestsPostRepositoriesUsernameRepoSlugPullrequestsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pullrequests', 'postRepositoriesUsernameRepoSlugPullrequests', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pullrequestsPullRequestId = 'fakedata';
    describe('#postRepositoriesUsernameRepoSlugPullrequestsPullRequestIdApprove - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugPullrequestsPullRequestIdApprove(pullrequestsUsername, pullrequestsPullRequestId, pullrequestsRepoSlug, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pullrequests', 'postRepositoriesUsernameRepoSlugPullrequestsPullRequestIdApprove', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRepositoriesUsernameRepoSlugPullrequestsPullRequestIdDecline - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugPullrequestsPullRequestIdDecline(pullrequestsUsername, pullrequestsPullRequestId, pullrequestsRepoSlug, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pullrequests', 'postRepositoriesUsernameRepoSlugPullrequestsPullRequestIdDecline', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pullrequestsPostRepositoriesUsernameRepoSlugPullrequestsPullRequestIdMergeBodyParam = {
      type: 'string'
    };
    describe('#postRepositoriesUsernameRepoSlugPullrequestsPullRequestIdMerge - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugPullrequestsPullRequestIdMerge(pullrequestsUsername, pullrequestsPullRequestId, pullrequestsRepoSlug, pullrequestsPostRepositoriesUsernameRepoSlugPullrequestsPullRequestIdMergeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pullrequests', 'postRepositoriesUsernameRepoSlugPullrequestsPullRequestIdMerge', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugDefaultReviewers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugDefaultReviewers(pullrequestsUsername, pullrequestsRepoSlug, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pullrequests', 'getRepositoriesUsernameRepoSlugDefaultReviewers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pullrequestsTargetUsername = 'fakedata';
    describe('#putRepositoriesUsernameRepoSlugDefaultReviewersTargetUsername - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putRepositoriesUsernameRepoSlugDefaultReviewersTargetUsername(pullrequestsUsername, pullrequestsTargetUsername, pullrequestsRepoSlug, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pullrequests', 'putRepositoriesUsernameRepoSlugDefaultReviewersTargetUsername', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugDefaultReviewersTargetUsername - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugDefaultReviewersTargetUsername(pullrequestsUsername, pullrequestsTargetUsername, pullrequestsRepoSlug, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pullrequests', 'getRepositoriesUsernameRepoSlugDefaultReviewersTargetUsername', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugPullrequests - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugPullrequests(pullrequestsUsername, pullrequestsRepoSlug, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pullrequests', 'getRepositoriesUsernameRepoSlugPullrequests', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugPullrequestsActivity - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugPullrequestsActivity(pullrequestsUsername, pullrequestsRepoSlug, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pullrequests', 'getRepositoriesUsernameRepoSlugPullrequestsActivity', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pullrequestsPutRepositoriesUsernameRepoSlugPullrequestsPullRequestIdBodyParam = {};
    describe('#putRepositoriesUsernameRepoSlugPullrequestsPullRequestId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putRepositoriesUsernameRepoSlugPullrequestsPullRequestId(pullrequestsUsername, pullrequestsRepoSlug, pullrequestsPullRequestId, pullrequestsPutRepositoriesUsernameRepoSlugPullrequestsPullRequestIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pullrequests', 'putRepositoriesUsernameRepoSlugPullrequestsPullRequestId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugPullrequestsPullRequestId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugPullrequestsPullRequestId(pullrequestsUsername, pullrequestsRepoSlug, pullrequestsPullRequestId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pullrequests', 'getRepositoriesUsernameRepoSlugPullrequestsPullRequestId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdActivity - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdActivity(pullrequestsUsername, pullrequestsRepoSlug, pullrequestsPullRequestId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pullrequests', 'getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdActivity', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdComments - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdComments(pullrequestsUsername, pullrequestsPullRequestId, pullrequestsRepoSlug, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pullrequests', 'getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdComments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pullrequestsCommentId = 'fakedata';
    describe('#getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommentsCommentId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommentsCommentId(pullrequestsUsername, pullrequestsPullRequestId, pullrequestsCommentId, pullrequestsRepoSlug, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pullrequests', 'getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommentsCommentId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommits - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommits(pullrequestsUsername, pullrequestsPullRequestId, pullrequestsRepoSlug, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pullrequests', 'getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommits', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdDiff - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdDiff(pullrequestsUsername, pullrequestsPullRequestId, pullrequestsRepoSlug, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pullrequests', 'getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdDiff', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdPatch - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdPatch(pullrequestsUsername, pullrequestsPullRequestId, pullrequestsRepoSlug, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pullrequests', 'getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdPatch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const downloadsUsername = 'fakedata';
    const downloadsRepoSlug = 'fakedata';
    describe('#postRepositoriesUsernameRepoSlugDownloads - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugDownloads(downloadsUsername, downloadsRepoSlug, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Downloads', 'postRepositoriesUsernameRepoSlugDownloads', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugDownloads - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugDownloads(downloadsUsername, downloadsRepoSlug, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Downloads', 'getRepositoriesUsernameRepoSlugDownloads', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const downloadsFilename = 'fakedata';
    describe('#getRepositoriesUsernameRepoSlugDownloadsFilename - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugDownloadsFilename(downloadsUsername, downloadsFilename, downloadsRepoSlug, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Downloads', 'getRepositoriesUsernameRepoSlugDownloadsFilename', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pipelinesUsername = 'fakedata';
    const pipelinesRepoSlug = 'fakedata';
    const pipelinesCreatePipelineForRepositoryBodyParam = {};
    describe('#createPipelineForRepository - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createPipelineForRepository(pipelinesUsername, pipelinesRepoSlug, pipelinesCreatePipelineForRepositoryBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pipelines', 'createPipelineForRepository', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pipelinesPipelineUuid = 'fakedata';
    describe('#stopPipeline - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.stopPipeline(pipelinesUsername, pipelinesRepoSlug, pipelinesPipelineUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pipelines', 'stopPipeline', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pipelinesCreateRepositoryPipelineKnownHostBodyParam = {
      type: 'string',
      hostname: 'string',
      public_key: {
        type: 'string',
        key: 'string',
        key_type: 'string',
        md5_fingerprint: 'string',
        sha256_fingerprint: 'string'
      },
      uuid: 'string'
    };
    describe('#createRepositoryPipelineKnownHost - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createRepositoryPipelineKnownHost(pipelinesUsername, pipelinesRepoSlug, pipelinesCreateRepositoryPipelineKnownHostBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.hostname);
                assert.equal('object', typeof data.response.public_key);
                assert.equal('string', data.response.uuid);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pipelines', 'createRepositoryPipelineKnownHost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pipelinesCreateRepositoryPipelineVariableBodyParam = {
      type: 'string',
      key: 'string',
      secured: true,
      uuid: 'string',
      value: 'string'
    };
    describe('#createRepositoryPipelineVariable - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createRepositoryPipelineVariable(pipelinesUsername, pipelinesRepoSlug, pipelinesCreateRepositoryPipelineVariableBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.key);
                assert.equal(true, data.response.secured);
                assert.equal('string', data.response.uuid);
                assert.equal('string', data.response.value);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pipelines', 'createRepositoryPipelineVariable', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pipelinesCreatePipelineVariableForTeamBodyParam = {
      type: 'string',
      key: 'string',
      secured: true,
      uuid: 'string',
      value: 'string'
    };
    describe('#createPipelineVariableForTeam - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createPipelineVariableForTeam(pipelinesUsername, pipelinesCreatePipelineVariableForTeamBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.key);
                assert.equal(true, data.response.secured);
                assert.equal('string', data.response.uuid);
                assert.equal('string', data.response.value);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pipelines', 'createPipelineVariableForTeam', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pipelinesCreatePipelineVariableForUserBodyParam = {
      type: 'string',
      key: 'string',
      secured: true,
      uuid: 'string',
      value: 'string'
    };
    describe('#createPipelineVariableForUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createPipelineVariableForUser(pipelinesUsername, pipelinesCreatePipelineVariableForUserBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.key);
                assert.equal(false, data.response.secured);
                assert.equal('string', data.response.uuid);
                assert.equal('string', data.response.value);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pipelines', 'createPipelineVariableForUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPipelinesForRepository - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPipelinesForRepository(pipelinesUsername, pipelinesRepoSlug, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pipelines', 'getPipelinesForRepository', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPipelineForRepository - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPipelineForRepository(pipelinesUsername, pipelinesRepoSlug, pipelinesPipelineUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pipelines', 'getPipelineForRepository', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPipelineStepsForRepository - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPipelineStepsForRepository(pipelinesUsername, pipelinesRepoSlug, pipelinesPipelineUuid, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.next);
                assert.equal(8, data.response.page);
                assert.equal(4, data.response.pagelen);
                assert.equal('string', data.response.previous);
                assert.equal(9, data.response.size);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pipelines', 'getPipelineStepsForRepository', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pipelinesStepUuid = 'fakedata';
    describe('#getPipelineStepForRepository - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPipelineStepForRepository(pipelinesUsername, pipelinesRepoSlug, pipelinesPipelineUuid, pipelinesStepUuid, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.completed_on);
                assert.equal('object', typeof data.response.image);
                assert.equal(10, data.response.logByteCount);
                assert.equal(true, Array.isArray(data.response.script_commands));
                assert.equal(true, Array.isArray(data.response.setup_commands));
                assert.equal('string', data.response.started_on);
                assert.equal('object', typeof data.response.state);
                assert.equal('string', data.response.uuid);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pipelines', 'getPipelineStepForRepository', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPipelineStepLogForRepository - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPipelineStepLogForRepository(pipelinesUsername, pipelinesRepoSlug, pipelinesPipelineUuid, pipelinesStepUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pipelines', 'getPipelineStepLogForRepository', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pipelinesUpdateRepositoryPipelineConfigBodyParam = {};
    describe('#updateRepositoryPipelineConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateRepositoryPipelineConfig(pipelinesUsername, pipelinesRepoSlug, pipelinesUpdateRepositoryPipelineConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pipelines', 'updateRepositoryPipelineConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoryPipelineConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRepositoryPipelineConfig(pipelinesUsername, pipelinesRepoSlug, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pipelines', 'getRepositoryPipelineConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pipelinesUpdateRepositoryPipelineKeyPairBodyParam = {
      type: 'string',
      private_key: 'string',
      public_key: 'string'
    };
    describe('#updateRepositoryPipelineKeyPair - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateRepositoryPipelineKeyPair(pipelinesUsername, pipelinesRepoSlug, pipelinesUpdateRepositoryPipelineKeyPairBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.private_key);
                assert.equal('string', data.response.public_key);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pipelines', 'updateRepositoryPipelineKeyPair', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoryPipelineSshKeyPair - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRepositoryPipelineSshKeyPair(pipelinesUsername, pipelinesRepoSlug, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.private_key);
                assert.equal('string', data.response.public_key);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pipelines', 'getRepositoryPipelineSshKeyPair', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoryPipelineKnownHosts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRepositoryPipelineKnownHosts(pipelinesUsername, pipelinesRepoSlug, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.next);
                assert.equal(8, data.response.page);
                assert.equal(9, data.response.pagelen);
                assert.equal('string', data.response.previous);
                assert.equal(10, data.response.size);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pipelines', 'getRepositoryPipelineKnownHosts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pipelinesKnownHostUuid = 'fakedata';
    const pipelinesUpdateRepositoryPipelineKnownHostBodyParam = {
      type: 'string',
      hostname: 'string',
      public_key: {
        type: 'string',
        key: 'string',
        key_type: 'string',
        md5_fingerprint: 'string',
        sha256_fingerprint: 'string'
      },
      uuid: 'string'
    };
    describe('#updateRepositoryPipelineKnownHost - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateRepositoryPipelineKnownHost(pipelinesUsername, pipelinesRepoSlug, pipelinesKnownHostUuid, pipelinesUpdateRepositoryPipelineKnownHostBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.hostname);
                assert.equal('object', typeof data.response.public_key);
                assert.equal('string', data.response.uuid);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pipelines', 'updateRepositoryPipelineKnownHost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoryPipelineKnownHost - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRepositoryPipelineKnownHost(pipelinesUsername, pipelinesRepoSlug, pipelinesKnownHostUuid, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.hostname);
                assert.equal('object', typeof data.response.public_key);
                assert.equal('string', data.response.uuid);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pipelines', 'getRepositoryPipelineKnownHost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoryPipelineVariables - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRepositoryPipelineVariables(pipelinesUsername, pipelinesRepoSlug, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.next);
                assert.equal(5, data.response.page);
                assert.equal(9, data.response.pagelen);
                assert.equal('string', data.response.previous);
                assert.equal(3, data.response.size);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pipelines', 'getRepositoryPipelineVariables', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pipelinesVariableUuid = 'fakedata';
    const pipelinesUpdateRepositoryPipelineVariableBodyParam = {
      type: 'string',
      key: 'string',
      secured: true,
      uuid: 'string',
      value: 'string'
    };
    describe('#updateRepositoryPipelineVariable - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateRepositoryPipelineVariable(pipelinesUsername, pipelinesRepoSlug, pipelinesVariableUuid, pipelinesUpdateRepositoryPipelineVariableBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.key);
                assert.equal(false, data.response.secured);
                assert.equal('string', data.response.uuid);
                assert.equal('string', data.response.value);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pipelines', 'updateRepositoryPipelineVariable', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoryPipelineVariable - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRepositoryPipelineVariable(pipelinesUsername, pipelinesRepoSlug, pipelinesVariableUuid, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.key);
                assert.equal(true, data.response.secured);
                assert.equal('string', data.response.uuid);
                assert.equal('string', data.response.value);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pipelines', 'getRepositoryPipelineVariable', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPipelineVariablesForTeam - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPipelineVariablesForTeam(pipelinesUsername, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.next);
                assert.equal(9, data.response.page);
                assert.equal(8, data.response.pagelen);
                assert.equal('string', data.response.previous);
                assert.equal(3, data.response.size);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pipelines', 'getPipelineVariablesForTeam', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pipelinesUpdatePipelineVariableForTeamBodyParam = {
      type: 'string',
      key: 'string',
      secured: true,
      uuid: 'string',
      value: 'string'
    };
    describe('#updatePipelineVariableForTeam - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatePipelineVariableForTeam(pipelinesUsername, pipelinesVariableUuid, pipelinesUpdatePipelineVariableForTeamBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.key);
                assert.equal(true, data.response.secured);
                assert.equal('string', data.response.uuid);
                assert.equal('string', data.response.value);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pipelines', 'updatePipelineVariableForTeam', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPipelineVariableForTeam - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPipelineVariableForTeam(pipelinesUsername, pipelinesVariableUuid, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.key);
                assert.equal(true, data.response.secured);
                assert.equal('string', data.response.uuid);
                assert.equal('string', data.response.value);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pipelines', 'getPipelineVariableForTeam', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPipelineVariablesForUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPipelineVariablesForUser(pipelinesUsername, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.next);
                assert.equal(2, data.response.page);
                assert.equal(2, data.response.pagelen);
                assert.equal('string', data.response.previous);
                assert.equal(8, data.response.size);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pipelines', 'getPipelineVariablesForUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pipelinesUpdatePipelineVariableForUserBodyParam = {
      type: 'string',
      key: 'string',
      secured: true,
      uuid: 'string',
      value: 'string'
    };
    describe('#updatePipelineVariableForUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatePipelineVariableForUser(pipelinesUsername, pipelinesVariableUuid, pipelinesUpdatePipelineVariableForUserBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.key);
                assert.equal(false, data.response.secured);
                assert.equal('string', data.response.uuid);
                assert.equal('string', data.response.value);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pipelines', 'updatePipelineVariableForUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPipelineVariableForUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPipelineVariableForUser(pipelinesUsername, pipelinesVariableUuid, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.key);
                assert.equal(true, data.response.secured);
                assert.equal('string', data.response.uuid);
                assert.equal('string', data.response.value);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pipelines', 'getPipelineVariableForUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const refsUsername = 'fakedata';
    const refsRepoSlug = 'fakedata';
    const refsPostRepositoriesUsernameRepoSlugRefsTagsBodyParam = {
      type: 'string'
    };
    describe('#postRepositoriesUsernameRepoSlugRefsTags - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postRepositoriesUsernameRepoSlugRefsTags(refsUsername, refsRepoSlug, refsPostRepositoriesUsernameRepoSlugRefsTagsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Refs', 'postRepositoriesUsernameRepoSlugRefsTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugRefs - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugRefs(refsUsername, refsRepoSlug, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Refs', 'getRepositoriesUsernameRepoSlugRefs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugRefsBranches - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugRefsBranches(refsUsername, refsRepoSlug, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Refs', 'getRepositoriesUsernameRepoSlugRefsBranches', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const refsName = 'fakedata';
    describe('#getRepositoriesUsernameRepoSlugRefsBranchesName - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugRefsBranchesName(refsUsername, refsName, refsRepoSlug, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Refs', 'getRepositoriesUsernameRepoSlugRefsBranchesName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugRefsTags - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugRefsTags(refsUsername, refsRepoSlug, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Refs', 'getRepositoriesUsernameRepoSlugRefsTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesUsernameRepoSlugRefsTagsName - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRepositoriesUsernameRepoSlugRefsTagsName(refsUsername, refsName, refsRepoSlug, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Refs', 'getRepositoriesUsernameRepoSlugRefsTagsName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let snippetsEncodedId = 'fakedata';
    let snippetsCommentId = 'fakedata';
    let snippetsNodeId = 'fakedata';
    const snippetsPostSnippetsBodyParam = {
      type: 'string',
      created_on: 'string',
      creator: {
        type: 'string',
        created_on: 'string',
        display_name: 'string',
        links: {
          avatar: {
            href: 'string'
          },
          followers: {
            href: 'string'
          },
          following: {
            href: 'string'
          },
          html: {
            href: 'string'
          },
          repositories: {
            href: 'string'
          },
          self: {
            href: 'string'
          }
        },
        username: 'string',
        uuid: 'string',
        website: 'string'
      },
      id: 4,
      is_private: false,
      owner: {
        type: 'string',
        created_on: 'string',
        display_name: 'string',
        links: {
          avatar: {
            href: 'string'
          },
          followers: {
            href: 'string'
          },
          following: {
            href: 'string'
          },
          html: {
            href: 'string'
          },
          repositories: {
            href: 'string'
          },
          self: {
            href: 'string'
          }
        },
        username: 'string',
        uuid: 'string',
        website: 'string'
      },
      scm: 'hg',
      title: 'string',
      updated_on: 'string'
    };
    describe('#postSnippets - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postSnippets(snippetsPostSnippetsBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.created_on);
                assert.equal('object', typeof data.response.creator);
                assert.equal(5, data.response.id);
                assert.equal(false, data.response.is_private);
                assert.equal('object', typeof data.response.owner);
                assert.equal('git', data.response.scm);
                assert.equal('string', data.response.title);
                assert.equal('string', data.response.updated_on);
              } else {
                runCommonAsserts(data, error);
              }
              snippetsEncodedId = data.response.id;
              snippetsCommentId = data.response.id;
              snippetsNodeId = data.response.id;
              saveMockData('Snippets', 'postSnippets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snippetsUsername = 'fakedata';
    const snippetsPostSnippetsUsernameBodyParam = {
      type: 'string',
      created_on: 'string',
      creator: {
        type: 'string',
        created_on: 'string',
        display_name: 'string',
        links: {
          avatar: {
            href: 'string'
          },
          followers: {
            href: 'string'
          },
          following: {
            href: 'string'
          },
          html: {
            href: 'string'
          },
          repositories: {
            href: 'string'
          },
          self: {
            href: 'string'
          }
        },
        username: 'string',
        uuid: 'string',
        website: 'string'
      },
      id: 5,
      is_private: false,
      owner: {
        type: 'string',
        created_on: 'string',
        display_name: 'string',
        links: {
          avatar: {
            href: 'string'
          },
          followers: {
            href: 'string'
          },
          following: {
            href: 'string'
          },
          html: {
            href: 'string'
          },
          repositories: {
            href: 'string'
          },
          self: {
            href: 'string'
          }
        },
        username: 'string',
        uuid: 'string',
        website: 'string'
      },
      scm: 'git',
      title: 'string',
      updated_on: 'string'
    };
    describe('#postSnippetsUsername - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postSnippetsUsername(snippetsUsername, snippetsPostSnippetsUsernameBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.created_on);
                assert.equal('object', typeof data.response.creator);
                assert.equal(8, data.response.id);
                assert.equal(false, data.response.is_private);
                assert.equal('object', typeof data.response.owner);
                assert.equal('git', data.response.scm);
                assert.equal('string', data.response.title);
                assert.equal('string', data.response.updated_on);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snippets', 'postSnippetsUsername', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snippetsPostSnippetsUsernameEncodedIdCommentsBodyParam = {
      type: 'string',
      created_on: 'string',
      creator: {
        type: 'string',
        created_on: 'string',
        display_name: 'string',
        links: {
          avatar: {
            href: 'string'
          },
          followers: {
            href: 'string'
          },
          following: {
            href: 'string'
          },
          html: {
            href: 'string'
          },
          repositories: {
            href: 'string'
          },
          self: {
            href: 'string'
          }
        },
        username: 'string',
        uuid: 'string',
        website: 'string'
      },
      id: 8,
      is_private: true,
      owner: {
        type: 'string',
        created_on: 'string',
        display_name: 'string',
        links: {
          avatar: {
            href: 'string'
          },
          followers: {
            href: 'string'
          },
          following: {
            href: 'string'
          },
          html: {
            href: 'string'
          },
          repositories: {
            href: 'string'
          },
          self: {
            href: 'string'
          }
        },
        username: 'string',
        uuid: 'string',
        website: 'string'
      },
      scm: 'git',
      title: 'string',
      updated_on: 'string'
    };
    describe('#postSnippetsUsernameEncodedIdComments - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postSnippetsUsernameEncodedIdComments(snippetsUsername, snippetsEncodedId, snippetsPostSnippetsUsernameEncodedIdCommentsBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.created_on);
                assert.equal('object', typeof data.response.creator);
                assert.equal(6, data.response.id);
                assert.equal(false, data.response.is_private);
                assert.equal('object', typeof data.response.owner);
                assert.equal('hg', data.response.scm);
                assert.equal('string', data.response.title);
                assert.equal('string', data.response.updated_on);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snippets', 'postSnippetsUsernameEncodedIdComments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSnippets - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSnippets(null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.next);
                assert.equal(2, data.response.page);
                assert.equal(3, data.response.pagelen);
                assert.equal('string', data.response.previous);
                assert.equal(1, data.response.size);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snippets', 'getSnippets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSnippetsUsername - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSnippetsUsername(null, snippetsUsername, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.next);
                assert.equal(1, data.response.page);
                assert.equal(9, data.response.pagelen);
                assert.equal('string', data.response.previous);
                assert.equal(9, data.response.size);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snippets', 'getSnippetsUsername', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putSnippetsUsernameEncodedId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putSnippetsUsernameEncodedId(snippetsUsername, snippetsEncodedId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.created_on);
                assert.equal('object', typeof data.response.creator);
                assert.equal(6, data.response.id);
                assert.equal(false, data.response.is_private);
                assert.equal('object', typeof data.response.owner);
                assert.equal('git', data.response.scm);
                assert.equal('string', data.response.title);
                assert.equal('string', data.response.updated_on);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snippets', 'putSnippetsUsernameEncodedId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSnippetsUsernameEncodedId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSnippetsUsernameEncodedId(snippetsUsername, snippetsEncodedId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.created_on);
                assert.equal('object', typeof data.response.creator);
                assert.equal(3, data.response.id);
                assert.equal(true, data.response.is_private);
                assert.equal('object', typeof data.response.owner);
                assert.equal('git', data.response.scm);
                assert.equal('string', data.response.title);
                assert.equal('string', data.response.updated_on);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snippets', 'getSnippetsUsernameEncodedId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSnippetsUsernameEncodedIdComments - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSnippetsUsernameEncodedIdComments(snippetsUsername, snippetsEncodedId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.next);
                assert.equal(10, data.response.page);
                assert.equal(9, data.response.pagelen);
                assert.equal('string', data.response.previous);
                assert.equal(1, data.response.size);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snippets', 'getSnippetsUsernameEncodedIdComments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putSnippetsUsernameEncodedIdCommentsCommentId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putSnippetsUsernameEncodedIdCommentsCommentId(snippetsUsername, snippetsCommentId, snippetsEncodedId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snippets', 'putSnippetsUsernameEncodedIdCommentsCommentId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSnippetsUsernameEncodedIdCommentsCommentId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSnippetsUsernameEncodedIdCommentsCommentId(snippetsUsername, snippetsCommentId, snippetsEncodedId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal('object', typeof data.response.links);
                assert.equal('object', typeof data.response.snippet);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snippets', 'getSnippetsUsernameEncodedIdCommentsCommentId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSnippetsUsernameEncodedIdCommits - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSnippetsUsernameEncodedIdCommits(snippetsUsername, snippetsEncodedId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snippets', 'getSnippetsUsernameEncodedIdCommits', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snippetsRevision = 'fakedata';
    describe('#getSnippetsUsernameEncodedIdCommitsRevision - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSnippetsUsernameEncodedIdCommitsRevision(snippetsUsername, snippetsEncodedId, snippetsRevision, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snippets', 'getSnippetsUsernameEncodedIdCommitsRevision', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putSnippetsUsernameEncodedIdWatch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putSnippetsUsernameEncodedIdWatch(snippetsUsername, snippetsEncodedId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.next);
                assert.equal(5, data.response.page);
                assert.equal(10, data.response.pagelen);
                assert.equal('string', data.response.previous);
                assert.equal(7, data.response.size);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snippets', 'putSnippetsUsernameEncodedIdWatch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSnippetsUsernameEncodedIdWatch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSnippetsUsernameEncodedIdWatch(snippetsUsername, snippetsEncodedId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.next);
                assert.equal(5, data.response.page);
                assert.equal(2, data.response.pagelen);
                assert.equal('string', data.response.previous);
                assert.equal(7, data.response.size);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snippets', 'getSnippetsUsernameEncodedIdWatch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSnippetsUsernameEncodedIdWatchers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSnippetsUsernameEncodedIdWatchers(snippetsUsername, snippetsEncodedId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.next);
                assert.equal(2, data.response.page);
                assert.equal(1, data.response.pagelen);
                assert.equal('string', data.response.previous);
                assert.equal(2, data.response.size);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snippets', 'getSnippetsUsernameEncodedIdWatchers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putSnippetsUsernameEncodedIdNodeId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putSnippetsUsernameEncodedIdNodeId(snippetsUsername, snippetsEncodedId, snippetsNodeId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.created_on);
                assert.equal('object', typeof data.response.creator);
                assert.equal(1, data.response.id);
                assert.equal(false, data.response.is_private);
                assert.equal('object', typeof data.response.owner);
                assert.equal('hg', data.response.scm);
                assert.equal('string', data.response.title);
                assert.equal('string', data.response.updated_on);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snippets', 'putSnippetsUsernameEncodedIdNodeId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSnippetsUsernameEncodedIdNodeId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSnippetsUsernameEncodedIdNodeId(snippetsUsername, snippetsEncodedId, snippetsNodeId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.created_on);
                assert.equal('object', typeof data.response.creator);
                assert.equal(10, data.response.id);
                assert.equal(true, data.response.is_private);
                assert.equal('object', typeof data.response.owner);
                assert.equal('git', data.response.scm);
                assert.equal('string', data.response.title);
                assert.equal('string', data.response.updated_on);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snippets', 'getSnippetsUsernameEncodedIdNodeId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snippetsPathParam = 'fakedata';
    describe('#getSnippetsUsernameEncodedIdNodeIdFilesPath - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSnippetsUsernameEncodedIdNodeIdFilesPath(snippetsUsername, snippetsPathParam, snippetsNodeId, snippetsEncodedId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snippets', 'getSnippetsUsernameEncodedIdNodeIdFilesPath', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSnippetsUsernameEncodedIdRevisionDiff - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSnippetsUsernameEncodedIdRevisionDiff(snippetsUsername, snippetsPathParam, snippetsEncodedId, snippetsRevision, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snippets', 'getSnippetsUsernameEncodedIdRevisionDiff', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSnippetsUsernameEncodedIdRevisionPatch - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSnippetsUsernameEncodedIdRevisionPatch(snippetsUsername, snippetsEncodedId, snippetsRevision, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snippets', 'getSnippetsUsernameEncodedIdRevisionPatch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let projectsOwner = 'fakedata';
    const projectsPostTeamsOwnerProjectsBodyParam = {
      type: 'string',
      created_on: 'string',
      description: 'string',
      is_private: false,
      key: 'string',
      links: {
        avatar: {
          href: 'string'
        },
        html: {
          href: 'string'
        }
      },
      name: 'string',
      owner: {
        type: 'string',
        created_on: 'string',
        display_name: 'string',
        links: {
          avatar: {
            href: 'string'
          },
          followers: {
            href: 'string'
          },
          following: {
            href: 'string'
          },
          html: {
            href: 'string'
          },
          repositories: {
            href: 'string'
          },
          self: {
            href: 'string'
          }
        },
        username: 'string',
        uuid: 'string',
        website: 'string'
      },
      updated_on: 'string',
      uuid: 'string'
    };
    describe('#postTeamsOwnerProjects - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTeamsOwnerProjects(projectsOwner, projectsPostTeamsOwnerProjectsBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.created_on);
                assert.equal('string', data.response.description);
                assert.equal(false, data.response.is_private);
                assert.equal('string', data.response.key);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.owner);
                assert.equal('string', data.response.updated_on);
                assert.equal('string', data.response.uuid);
              } else {
                runCommonAsserts(data, error);
              }
              projectsOwner = data.response.owner;
              saveMockData('Projects', 'postTeamsOwnerProjects', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTeamsOwnerProjects - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTeamsOwnerProjects(projectsOwner, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.next);
                assert.equal(10, data.response.page);
                assert.equal(4, data.response.pagelen);
                assert.equal('string', data.response.previous);
                assert.equal(10, data.response.size);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Projects', 'getTeamsOwnerProjects', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const projectsProjectKey = 'fakedata';
    const projectsPutTeamsOwnerProjectsProjectKeyBodyParam = {
      type: 'string',
      created_on: 'string',
      description: 'string',
      is_private: true,
      key: 'string',
      links: {
        avatar: {
          href: 'string'
        },
        html: {
          href: 'string'
        }
      },
      name: 'string',
      owner: {
        type: 'string',
        created_on: 'string',
        display_name: 'string',
        links: {
          avatar: {
            href: 'string'
          },
          followers: {
            href: 'string'
          },
          following: {
            href: 'string'
          },
          html: {
            href: 'string'
          },
          repositories: {
            href: 'string'
          },
          self: {
            href: 'string'
          }
        },
        username: 'string',
        uuid: 'string',
        website: 'string'
      },
      updated_on: 'string',
      uuid: 'string'
    };
    describe('#putTeamsOwnerProjectsProjectKey - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putTeamsOwnerProjectsProjectKey(projectsOwner, projectsProjectKey, projectsPutTeamsOwnerProjectsProjectKeyBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.created_on);
                assert.equal('string', data.response.description);
                assert.equal(false, data.response.is_private);
                assert.equal('string', data.response.key);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.owner);
                assert.equal('string', data.response.updated_on);
                assert.equal('string', data.response.uuid);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Projects', 'putTeamsOwnerProjectsProjectKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTeamsOwnerProjectsProjectKey - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTeamsOwnerProjectsProjectKey(projectsOwner, projectsProjectKey, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.created_on);
                assert.equal('string', data.response.description);
                assert.equal(false, data.response.is_private);
                assert.equal('string', data.response.key);
                assert.equal('object', typeof data.response.links);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.owner);
                assert.equal('string', data.response.updated_on);
                assert.equal('string', data.response.uuid);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Projects', 'getTeamsOwnerProjectsProjectKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAddon - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAddon((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Addon', 'deleteAddon', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAddonLinkersLinkerKeyValues - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAddonLinkersLinkerKeyValues(addonLinkerKey, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Addon', 'deleteAddonLinkersLinkerKeyValues', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAddonLinkersLinkerKeyValues2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAddonLinkersLinkerKeyValues2(addonLinkerKey, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Addon', 'deleteAddonLinkersLinkerKeyValues2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRepositoriesUsernameRepoSlug - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRepositoriesUsernameRepoSlug(repositoriesUsername, repositoriesRepoSlug, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Repositories', 'deleteRepositoriesUsernameRepoSlug', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRepositoriesUsernameRepoSlugHooksUid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRepositoriesUsernameRepoSlugHooksUid(repositoriesUsername, repositoriesRepoSlug, repositoriesUid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Repositories', 'deleteRepositoriesUsernameRepoSlugHooksUid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTeamsUsernameHooksUid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTeamsUsernameHooksUid(teamsUsername, teamsUid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'deleteTeamsUsernameHooksUid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUsersUsernameHooksUid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteUsersUsernameHooksUid(usersUsername, usersUid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'deleteUsersUsernameHooksUid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRepositoriesUsernameRepoSlugBranchRestrictionsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRepositoriesUsernameRepoSlugBranchRestrictionsId(branchrestrictionsUsername, branchrestrictionsRepoSlug, branchrestrictionsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Branchrestrictions', 'deleteRepositoriesUsernameRepoSlugBranchRestrictionsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRepositoriesUsernameRepoSlugCommitNodeApprove - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRepositoriesUsernameRepoSlugCommitNodeApprove(commitsUsername, commitsRepoSlug, commitsNode, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Commits', 'deleteRepositoriesUsernameRepoSlugCommitNodeApprove', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRepositoriesUsernameRepoSlugIssuesIssueId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRepositoriesUsernameRepoSlugIssuesIssueId(issueTrackerUsername, issueTrackerIssueId, issueTrackerRepoSlug, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueTracker', 'deleteRepositoriesUsernameRepoSlugIssuesIssueId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPath - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPath(issueTrackerUsername, issueTrackerPathParam, issueTrackerIssueId, issueTrackerRepoSlug, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueTracker', 'deleteRepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPath', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRepositoriesUsernameRepoSlugIssuesIssueIdVote - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRepositoriesUsernameRepoSlugIssuesIssueIdVote(issueTrackerUsername, issueTrackerRepoSlug, issueTrackerIssueId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueTracker', 'deleteRepositoriesUsernameRepoSlugIssuesIssueIdVote', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRepositoriesUsernameRepoSlugIssuesIssueIdWatch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteRepositoriesUsernameRepoSlugIssuesIssueIdWatch(issueTrackerUsername, issueTrackerRepoSlug, issueTrackerIssueId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.error);
                assert.equal('string', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IssueTracker', 'deleteRepositoriesUsernameRepoSlugIssuesIssueIdWatch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRepositoriesUsernameRepoSlugDefaultReviewersTargetUsername - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRepositoriesUsernameRepoSlugDefaultReviewersTargetUsername(pullrequestsUsername, pullrequestsTargetUsername, pullrequestsRepoSlug, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pullrequests', 'deleteRepositoriesUsernameRepoSlugDefaultReviewersTargetUsername', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRepositoriesUsernameRepoSlugPullrequestsPullRequestIdApprove - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRepositoriesUsernameRepoSlugPullrequestsPullRequestIdApprove(pullrequestsUsername, pullrequestsPullRequestId, pullrequestsRepoSlug, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pullrequests', 'deleteRepositoriesUsernameRepoSlugPullrequestsPullRequestIdApprove', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRepositoriesUsernameRepoSlugDownloadsFilename - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRepositoriesUsernameRepoSlugDownloadsFilename(downloadsUsername, downloadsFilename, downloadsRepoSlug, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Downloads', 'deleteRepositoriesUsernameRepoSlugDownloadsFilename', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRepositoryPipelineKeyPair - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRepositoryPipelineKeyPair(pipelinesUsername, pipelinesRepoSlug, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pipelines', 'deleteRepositoryPipelineKeyPair', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRepositoryPipelineKnownHost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRepositoryPipelineKnownHost(pipelinesUsername, pipelinesRepoSlug, pipelinesKnownHostUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pipelines', 'deleteRepositoryPipelineKnownHost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRepositoryPipelineVariable - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRepositoryPipelineVariable(pipelinesUsername, pipelinesRepoSlug, pipelinesVariableUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pipelines', 'deleteRepositoryPipelineVariable', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePipelineVariableForTeam - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePipelineVariableForTeam(pipelinesUsername, pipelinesVariableUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pipelines', 'deletePipelineVariableForTeam', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePipelineVariableForUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePipelineVariableForUser(pipelinesUsername, pipelinesVariableUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pipelines', 'deletePipelineVariableForUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSnippetsUsernameEncodedId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSnippetsUsernameEncodedId(snippetsUsername, snippetsEncodedId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snippets', 'deleteSnippetsUsernameEncodedId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSnippetsUsernameEncodedIdCommentsCommentId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSnippetsUsernameEncodedIdCommentsCommentId(snippetsUsername, snippetsCommentId, snippetsEncodedId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snippets', 'deleteSnippetsUsernameEncodedIdCommentsCommentId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSnippetsUsernameEncodedIdWatch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteSnippetsUsernameEncodedIdWatch(snippetsUsername, snippetsEncodedId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.next);
                assert.equal(2, data.response.page);
                assert.equal(10, data.response.pagelen);
                assert.equal('string', data.response.previous);
                assert.equal(6, data.response.size);
                assert.equal(true, Array.isArray(data.response.values));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snippets', 'deleteSnippetsUsernameEncodedIdWatch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSnippetsUsernameEncodedIdNodeId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSnippetsUsernameEncodedIdNodeId(snippetsUsername, snippetsNodeId, snippetsEncodedId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snippets', 'deleteSnippetsUsernameEncodedIdNodeId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTeamsOwnerProjectsProjectKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTeamsOwnerProjectsProjectKey(projectsOwner, projectsProjectKey, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bitbucket-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Projects', 'deleteTeamsOwnerProjectsProjectKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
