## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Bitbucket. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Bitbucket.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Bitbucket. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">deleteAddon(callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/addon?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putAddon(callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/addon?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAddonLinkers(callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/addon/linkers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAddonLinkersLinkerKey(linkerKey, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/addon/linkers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAddonLinkersLinkerKeyValues(linkerKey, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/addon/linkers/{pathv1}/values?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAddonLinkersLinkerKeyValues(linkerKey, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/addon/linkers/{pathv1}/values?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddonLinkersLinkerKeyValues(linkerKey, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/addon/linkers/{pathv1}/values?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putAddonLinkersLinkerKeyValues(linkerKey, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/addon/linkers/{pathv1}/values?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAddonLinkersLinkerKeyValues2(linkerKey, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/addon/linkers/{pathv1}/values/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAddonLinkersLinkerKeyValues2(linkerKey, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/addon/linkers/{pathv1}/values/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHookEvents(callback)</td>
    <td style="padding:15px">Returns the webhook resource or subject types on which webhooks can
be registered.
Each resource/subject type contains an `events` link that returns the
paginated list of specific events each individual subject type can
emit.
This endpoint is publicly accessible and does not require
authentication or scopes.
Example:
```
$ curl https://api.bitbucket.org/2.0/hook_events
{
    "repository": {
        "links": {
            "events": {
                "href": "https://api.bitbucket.org/2.0/hook_eve...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/hook_events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHookEventsSubjectType(subjectType = 'user', callback)</td>
    <td style="padding:15px">Returns a paginated list of all valid webhook events for the
specified entity.
This is public data that does not require any scopes or authentication.
Example:
NOTE: The following example is a truncated response object for the `team` `subject_type`.
We return the same structure for the other `subject_type` objects.
```
$ curl https://api.bitbucket.org/2.0/hook_events/team
{
    "page": 1,
    "pagelen": 30,
    "size": 21,
    "values": [
        {
            "category": "Repository",
         ...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/hook_events/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugHooks(username, repoSlug, callback)</td>
    <td style="padding:15px">Returns a paginated list of webhooks installed on this repository.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/hooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRepositoriesUsernameRepoSlugHooks(username, repoSlug, callback)</td>
    <td style="padding:15px">Creates a new webhook on the specified repository.
Example:
```
$ curl -X POST -u credentials -H 'Content-Type: application/json'           https://api.bitbucket.org/2.0/repositories/username/slug/hooks           -d '
    {
      "description": "Webhook Description",
      "url": "https://example.com/",
      "active": true,
      "events": [
        "repo:push",
        "issue:created",
        "issue:updated"
      ]
    }'
```
Note that this call requires the webhook scope, as well as any sco...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/hooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRepositoriesUsernameRepoSlugHooksUid(username, repoSlug, uid, callback)</td>
    <td style="padding:15px">Deletes the specified webhook subscription from the given
repository.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/hooks/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugHooksUid(username, repoSlug, uid, callback)</td>
    <td style="padding:15px">Returns the webhook with the specified id installed on the specified
repository.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/hooks/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRepositoriesUsernameRepoSlugSrc(username, repoSlug, body, filename, attribute, callback)</td>
    <td style="padding:15px">Creates a new repository.
Note: In order to set the project for the newly created repository,
pass in either the project key or the project UUID as part of the
request body as shown in the examples below:
```
$ curl -X POST -H "Content-Type: application/json" -d '{
    "scm": "git",
    "project": {
        "key": "MARS"
    }
}' https://api.bitbucket.org/2.0/repositories/teamsinspace/hablanding
```
or
```
$ curl -X POST -H "Content-Type: application/json" -d '{
    "scm": "git",
    "project": ...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/src?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">CreateACommitByUploadingAFile(username, repoSlug, message, author, parents, files, branch, callback)</td>
    <td style="padding:15px">This endpoint is used to create new commits in the repository by uploading files.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/src?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRepositoriesUsernameRepoSlugHooksUid(username, repoSlug, uid, callback)</td>
    <td style="padding:15px">Updates the specified webhook subscription.
The following properties can be mutated:
* `description`
* `url`
* `active`
* `events`</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/hooks/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositories(after, callback)</td>
    <td style="padding:15px">Returns a paginated list of all public repositories.
This endpoint also supports filtering and sorting of the results. See
[filtering and sorting](../meta/filtering) for more details.</td>
    <td style="padding:15px">{base_path}/{version}/repositories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsername(username, role = 'admin', callback)</td>
    <td style="padding:15px">Returns a paginated list of all repositories owned by the specified
account or UUID.
The result can be narrowed down based on the authenticated user's role.
E.g. with `?role=contributor`, only those repositories that the
authenticated user has write access to are returned (this includes any
repo the user is an admin on, as that implies write access).
This endpoint also supports filtering and sorting of the results. See
[filtering and sorting](../../meta/filtering) for more details.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRepositoriesUsernameRepoSlug(username, repoSlug, callback)</td>
    <td style="padding:15px">Deletes the repository. This is an irreversible operation.
This does not affect its forks.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlug(username, repoSlug, callback)</td>
    <td style="padding:15px">Returns the object describing this repository.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRepositoriesUsernameRepoSlug(username, repoSlug, body, callback)</td>
    <td style="padding:15px">Creates a new repository.
Note: In order to set the project for the newly created repository,
pass in either the project key or the project UUID as part of the
request body as shown in the examples below:
```
$ curl -X POST -H "Content-Type: application/json" -d '{
    "scm": "git",
    "project": {
        "key": "MARS"
    }
}' https://api.bitbucket.org/2.0/repositories/teamsinspace/hablanding
```
or
```
$ curl -X POST -H "Content-Type: application/json" -d '{
    "scm": "git",
    "project": ...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRepositoriesUsernameRepoSlug(username, repoSlug, body, callback)</td>
    <td style="padding:15px">Since this endpoint can be used to both update and to create a
repository, the request body depends on the intent.
### Creation
See the POST documentation for the repository endpoint for an example
of the request body.
### Update
Note: Changing the `name` of the repository will cause the location to
be changed. This is because the URL of the repo is derived from the
name (a process called slugification). In such a scenario, it is
possible for the request to fail if the newly created slug conflic...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugCommitNodeStatuses(username, repoSlug, node, callback)</td>
    <td style="padding:15px">Returns all statuses (e.g. build results) for a specific commit.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/commit/{pathv3}/statuses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRepositoriesUsernameRepoSlugCommitNodeStatusesBuild(username, repoSlug, node, callback)</td>
    <td style="padding:15px">Creates a new build status against the specified commit.
If the specified key already exists, the existing status object will
be overwritten.
When creating a new commit status, you can use a URI template for the URL.
Templates are URLs that contain variable names that Bitbucket will
evaluate at runtime whenever the URL is displayed anywhere similar to
parameter substitution in
[Bitbucket Connect](https://developer.atlassian.com/bitbucket/concepts/context-parameters.html).
For example, one could ...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/commit/{pathv3}/statuses/build?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugCommitNodeStatusesBuildKey(username, repoSlug, node, key, callback)</td>
    <td style="padding:15px">Returns the specified build status for a commit.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/commit/{pathv3}/statuses/build/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRepositoriesUsernameRepoSlugCommitNodeStatusesBuildKey(username, repoSlug, node, key, body, callback)</td>
    <td style="padding:15px">Used to update the current status of a build status object on the
specific commit.
This operation can also be used to change other properties of the
build status:
* `state`
* `name`
* `description`
* `url`
* `refname`
The `key` cannot be changed.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/commit/{pathv3}/statuses/build/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugForks(username, repoSlug, callback)</td>
    <td style="padding:15px">Returns a paginated list of all the forks of the specified
repository.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/forks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdStatuses(username, repoSlug, pullRequestId, callback)</td>
    <td style="padding:15px">Returns all statuses (e.g. build results) for the given pull
request.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/pullrequests/{pathv3}/statuses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugWatchers(username, repoSlug, callback)</td>
    <td style="padding:15px">Returns a paginated list of all the watchers on the specified
repository.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/watchers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTeamsUsernameHooks(username, callback)</td>
    <td style="padding:15px">Returns a paginated list of webhooks installed on this team.</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/hooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTeamsUsernameHooks(username, callback)</td>
    <td style="padding:15px">Creates a new webhook on the specified team.
Team webhooks are fired for events from all repositories belonging to
that team account.
Note that only admins can install webhooks on teams.</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/hooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTeamsUsernameHooksUid(username, uid, callback)</td>
    <td style="padding:15px">Deletes the specified webhook subscription from the given team
account.</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/hooks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTeamsUsernameHooksUid(username, uid, callback)</td>
    <td style="padding:15px">Returns the webhook with the specified id installed on the given
team account.</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/hooks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTeamsUsernameHooksUid(username, uid, callback)</td>
    <td style="padding:15px">Updates the specified webhook subscription.
The following properties can be mutated:
* `description`
* `url`
* `active`
* `events`</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/hooks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTeams(role = 'admin', callback)</td>
    <td style="padding:15px">Returns all the teams that the authenticated user is associated
with.</td>
    <td style="padding:15px">{base_path}/{version}/teams?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTeamsUsername(username, callback)</td>
    <td style="padding:15px">Gets the public information associated with a team.
If the team's profile is private, `location`, `website` and
`created_on` elements are omitted.</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTeamsUsernameFollowers(username, callback)</td>
    <td style="padding:15px">Returns the list of accounts that are following this team.</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/followers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTeamsUsernameFollowing(username, callback)</td>
    <td style="padding:15px">Returns the list of accounts this team is following.</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/following?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTeamsUsernameMembers(username, callback)</td>
    <td style="padding:15px">All members of a team.
Returns all members of the specified team. Any member of any of the
team's groups is considered a member of the team. This includes users
in groups that may not actually have access to any of the team's
repositories.
Note that members using the "private profile" feature are not included.</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersUsernameHooks(username, callback)</td>
    <td style="padding:15px">Returns a paginated list of webhooks installed on this user account.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/hooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUsersUsernameHooks(username, callback)</td>
    <td style="padding:15px">Creates a new webhook on the specified user account.
Account-level webhooks are fired for events from all repositories
belonging to that account.
Note that one can only register webhooks on one's own account, not that
of others.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/hooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUsersUsernameHooksUid(username, uid, callback)</td>
    <td style="padding:15px">Deletes the specified webhook subscription from the given user
account.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/hooks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersUsernameHooksUid(username, uid, callback)</td>
    <td style="padding:15px">Returns the webhook with the specified id installed on the given
user account.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/hooks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putUsersUsernameHooksUid(username, uid, callback)</td>
    <td style="padding:15px">Updates the specified webhook subscription.
The following properties can be mutated:
* `description`
* `url`
* `active`
* `events`</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/hooks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTeamsUsernameRepositories(username, callback)</td>
    <td style="padding:15px">All repositories owned by a user/team. This includes private
repositories, but filtered down to the ones that the calling user has
access to.</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/repositories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersUsernameRepositories(username, callback)</td>
    <td style="padding:15px">All repositories owned by a user/team. This includes private
repositories, but filtered down to the ones that the calling user has
access to.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/repositories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUser(callback)</td>
    <td style="padding:15px">Returns the currently logged in user.</td>
    <td style="padding:15px">{base_path}/{version}/user?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserEmails(callback)</td>
    <td style="padding:15px">Returns all the authenticated user's email addresses. Both
confirmed and unconfirmed.</td>
    <td style="padding:15px">{base_path}/{version}/user/emails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserEmailsEmail(email, callback)</td>
    <td style="padding:15px">Returns details about a specific one of the authenticated user's
email addresses.
Details describe whether the address has been confirmed by the user and
whether it is the user's primary address or not.</td>
    <td style="padding:15px">{base_path}/{version}/user/emails/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersUsername(username, callback)</td>
    <td style="padding:15px">Gets the public information associated with a user account.
If the user's profile is private, `location`, `website` and
`created_on` elements are omitted.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersUsernameFollowers(username, callback)</td>
    <td style="padding:15px">Returns the list of accounts that are following this team.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/followers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersUsernameFollowing(username, callback)</td>
    <td style="padding:15px">Returns the list of accounts this user is following.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/following?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugSrcNodePath(username, node, pathParam, repoSlug, format = 'meta', callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/src/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugBranchRestrictions(username, repoSlug, callback)</td>
    <td style="padding:15px">Returns a paginated list of all branch restrictions on the
repository.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/branch-restrictions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRepositoriesUsernameRepoSlugBranchRestrictions(username, repoSlug, body, callback)</td>
    <td style="padding:15px">Creates a new branch restriction rule for a repository.
`kind` describes what will be restricted. Allowed values are: `push`,
`force`, `delete`, and `restrict_merges`.
Different kinds of branch restrictions have different requirements:
* `push` and `restrict_merges` require `users` and `groups` to be
  specified. Empty lists are allowed, in which case permission is
  denied for everybody.
* `force` can not be specified in a Mercurial repository.
`pattern` is used to determine which branches will...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/branch-restrictions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRepositoriesUsernameRepoSlugBranchRestrictionsId(username, repoSlug, id, callback)</td>
    <td style="padding:15px">Deletes an existing branch restriction rule.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/branch-restrictions/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugBranchRestrictionsId(username, repoSlug, id, callback)</td>
    <td style="padding:15px">Returns a specific branch restriction rule.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/branch-restrictions/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRepositoriesUsernameRepoSlugBranchRestrictionsId(username, repoSlug, id, body, callback)</td>
    <td style="padding:15px">Updates an existing branch restriction rule.
Fields not present in the request body are ignored.
See [`POST`](../../branch-restrictions#post) for details.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/branch-restrictions/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRepositoriesUsernameRepoSlugCommitNodeApprove(username, repoSlug, node, callback)</td>
    <td style="padding:15px">Redact the authenticated user's approval of the specified commit.
This operation is only available to users that have explicit access to
the repository. In contrast, just the fact that a repository is
publicly accessible to users does not give them the ability to approve
commits.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/commit/{pathv3}/approve?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRepositoriesUsernameRepoSlugCommitNodeApprove(username, repoSlug, node, callback)</td>
    <td style="padding:15px">Approve the specified commit as the authenticated user.
This operation is only available to users that have explicit access to
the repository. In contrast, just the fact that a repository is
publicly accessible to users does not give them the ability to approve
commits.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/commit/{pathv3}/approve?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugCommitRevision(username, repoSlug, revision, callback)</td>
    <td style="padding:15px">Returns the specified commit.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/commit/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugCommitShaComments(username, sha, repoSlug, callback)</td>
    <td style="padding:15px">Returns the commit's comments.
This includes both global and inline comments.
The default sorting is oldest to newest and can be overridden with
the `sort` query parameter.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/commit/{pathv3}/comments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugCommitShaCommentsCommentId(username, sha, commentId, repoSlug, callback)</td>
    <td style="padding:15px">Returns the specified commit comment.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/commit/{pathv3}/comments/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugCommits(username, repoSlug, callback)</td>
    <td style="padding:15px">These are the repository's commits. They are paginated and returned
in reverse chronological order, similar to the output of `git log` and
`hg log`. Like these tools, the DAG can be filtered.
## GET /repositories/{username}/{repo_slug}/commits/
Returns all commits in the repo in topological order (newest commit
first). All branches and tags are included (similar to
`git log --all` and `hg log`).
## GET /repositories/{username}/{repo_slug}/commits/master
Returns all commits on rev `master` (simil...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/commits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRepositoriesUsernameRepoSlugCommits(username, repoSlug, callback)</td>
    <td style="padding:15px">Identical to `GET /repositories/{username}/{repo_slug}/commits`,
except that POST allows clients to place the include and exclude
parameters in the request body to avoid URL length issues.
**Note that this resource does NOT support new commit creation.**</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/commits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugCommitsRevision(username, revision, repoSlug, callback)</td>
    <td style="padding:15px">These are the repository's commits. They are paginated and returned
in reverse chronological order, similar to the output of `git log` and
`hg log`. Like these tools, the DAG can be filtered.
## GET /repositories/{username}/{repo_slug}/commits/
Returns all commits in the repo in topological order (newest commit
first). All branches and tags are included (similar to
`git log --all` and `hg log`).
## GET /repositories/{username}/{repo_slug}/commits/master
Returns all commits on rev `master` (simil...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/commits/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRepositoriesUsernameRepoSlugCommitsRevision(username, revision, repoSlug, callback)</td>
    <td style="padding:15px">Identical to `GET /repositories/{username}/{repo_slug}/commits`,
except that POST allows clients to place the include and exclude
parameters in the request body to avoid URL length issues.
**Note that this resource does NOT support new commit creation.**</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/commits/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugDiffSpec(username, spec, repoSlug, context, pathParam, callback)</td>
    <td style="padding:15px">Produces a raw, git-style diff for either a single commit (diffed
against its first parent), or a revspec of 2 commits (e.g.
`3a8b42..9ff173` where the first commit represents the source and the
second commit the destination).
In case of the latter (diffing a revspec), a 3-way diff, or merge diff,
is computed. This shows the changes introduced by the left branch
(`3a8b42` in our example) as compared againt the right branch
(`9ff173`).
This is equivalent to merging the left branch into the right ...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/diff/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugPatchSpec(username, spec, repoSlug, callback)</td>
    <td style="padding:15px">Produces a raw patch for a single commit (diffed against its first
parent), or a patch-series for a revspec of 2 commits (e.g.
`3a8b42..9ff173` where the first commit represents the source and the
second commit the destination).
In case of the latter (diffing a revspec), a patch series is returned
for the commits on the source branch (`3a8b42` and its ancestors in
our example). For Mercurial, a single patch is returned that combines
the changes of all commits on the source branch.
While similar ...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/patch/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugComponents(username, repoSlug, callback)</td>
    <td style="padding:15px">Returns the components that have been defined in the issue tracker.
This resource is only available on repositories that have the issue
tracker enabled.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/components?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugComponentsComponentId(username, repoSlug, componentId, callback)</td>
    <td style="padding:15px">Returns the specified issue tracker component object.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/components/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugIssues(username, repoSlug, callback)</td>
    <td style="padding:15px">Returns the issues in the issue tracker.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/issues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRepositoriesUsernameRepoSlugIssues(username, repoSlug, body, callback)</td>
    <td style="padding:15px">Creates a new issue.
This call requires authentication. Private repositories or private
issue trackers require the caller to authenticate with an account that
has appropriate authorisation.
The authenticated user is used for the issue's `reporter` field.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/issues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRepositoriesUsernameRepoSlugIssuesIssueId(username, issueId, repoSlug, callback)</td>
    <td style="padding:15px">Deletes the specified issue. This requires write access to the
repository.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/issues/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugIssuesIssueId(username, issueId, repoSlug, callback)</td>
    <td style="padding:15px">Returns the specified issue.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/issues/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugIssuesIssueIdAttachments(username, repoSlug, issueId, callback)</td>
    <td style="padding:15px">Returns all attachments for this issue.
This returns the files' meta data. This does not return the files'
actual contents.
The files are always ordered by their upload date.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/issues/{pathv3}/attachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRepositoriesUsernameRepoSlugIssuesIssueIdAttachments(username, repoSlug, issueId, callback)</td>
    <td style="padding:15px">Upload new issue attachments.
To upload files, perform a `multipart/form-data` POST containing one
or more file fields.
When a file is uploaded with the same name as an existing attachment,
then the existing file will be replaced.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/issues/{pathv3}/attachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPath(username, pathParam, issueId, repoSlug, callback)</td>
    <td style="padding:15px">Deletes an attachment.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/issues/{pathv3}/attachments/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugIssuesIssueIdAttachmentsPath(username, pathParam, issueId, repoSlug, callback)</td>
    <td style="padding:15px">Returns the contents of the specified file attachment.
Note that this endpoint does not return a JSON response, but instead
returns a redirect pointing to the actual file that in turn will return
the raw contents.
The redirect URL contains a one-time token that has a limited lifetime.
As a result, the link should not be persisted, stored, or shared.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/issues/{pathv3}/attachments/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugIssuesIssueIdComments(username, issueId, repoSlug, callback)</td>
    <td style="padding:15px">Returns all comments that were made on the specified issue.
The default sorting is oldest to newest and can be overridden with
the `sort` query parameter.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/issues/{pathv3}/comments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugIssuesIssueIdCommentsCommentId(username, commentId, issueId, repoSlug, callback)</td>
    <td style="padding:15px">Returns the specified issue comment object.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/issues/{pathv3}/comments/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRepositoriesUsernameRepoSlugIssuesIssueIdVote(username, repoSlug, issueId, callback)</td>
    <td style="padding:15px">Retract your vote.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/issues/{pathv3}/vote?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugIssuesIssueIdVote(username, repoSlug, issueId, callback)</td>
    <td style="padding:15px">Check whether the authenticated user has voted for this issue.
A 204 status code indicates that the user has voted, while a 404
implies they haven't.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/issues/{pathv3}/vote?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRepositoriesUsernameRepoSlugIssuesIssueIdVote(username, repoSlug, issueId, callback)</td>
    <td style="padding:15px">Vote for this issue.
To cast your vote, do an empty PUT. The 204 status code indicates that
the operation was successful.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/issues/{pathv3}/vote?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRepositoriesUsernameRepoSlugIssuesIssueIdWatch(username, repoSlug, issueId, callback)</td>
    <td style="padding:15px">Stop watching this issue.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/issues/{pathv3}/watch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugIssuesIssueIdWatch(username, repoSlug, issueId, callback)</td>
    <td style="padding:15px">Indicated whether or not the authenticated user is watching this
issue.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/issues/{pathv3}/watch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRepositoriesUsernameRepoSlugIssuesIssueIdWatch(username, repoSlug, issueId, callback)</td>
    <td style="padding:15px">Start watching this issue.
To start watching this issue, do an empty PUT. The 204 status code
indicates that the operation was successful.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/issues/{pathv3}/watch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugMilestones(username, repoSlug, callback)</td>
    <td style="padding:15px">Returns the milestones that have been defined in the issue tracker.
This resource is only available on repositories that have the issue
tracker enabled.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/milestones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugMilestonesMilestoneId(username, repoSlug, milestoneId, callback)</td>
    <td style="padding:15px">Returns the specified issue tracker milestone object.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/milestones/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugVersions(username, repoSlug, callback)</td>
    <td style="padding:15px">Returns the versions that have been defined in the issue tracker.
This resource is only available on repositories that have the issue
tracker enabled.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/versions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugVersionsVersionId(username, repoSlug, versionId, callback)</td>
    <td style="padding:15px">Returns the specified issue tracker version object.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/versions/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugDefaultReviewers(username, repoSlug, callback)</td>
    <td style="padding:15px">Returns the repository's default reviewers.
These are the users that are automatically added as reviewers on every
new pull request that is created.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/default-reviewers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRepositoriesUsernameRepoSlugDefaultReviewersTargetUsername(username, targetUsername, repoSlug, callback)</td>
    <td style="padding:15px">Removes a default reviewer from the repository.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/default-reviewers/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugDefaultReviewersTargetUsername(username, targetUsername, repoSlug, callback)</td>
    <td style="padding:15px">Returns the specified reviewer.
This can be used to test whether a user is among the repository's
default reviewers list. A 404 indicates that that specified user is not
a default reviewer.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/default-reviewers/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRepositoriesUsernameRepoSlugDefaultReviewersTargetUsername(username, targetUsername, repoSlug, callback)</td>
    <td style="padding:15px">Adds the specified user to the repository's list of default
reviewers.
This method is idempotent. Adding a user a second time has no effect.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/default-reviewers/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugPullrequests(username, repoSlug, state = 'MERGED', callback)</td>
    <td style="padding:15px">Returns a paginated list of all pull requests on the specified
repository. By default only open pull requests are returned. This can
be controlled using the `state` query parameter. To retrieve pull
requests that are in one of multiple states, repeat the `state`
parameter for each individual state.
This endpoint also supports filtering and sorting of the results. See
[filtering and sorting](../../../../meta/filtering) for more details.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/pullrequests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRepositoriesUsernameRepoSlugPullrequests(username, repoSlug, body, callback)</td>
    <td style="padding:15px">Creates a new pull request.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/pullrequests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugPullrequestsActivity(username, repoSlug, callback)</td>
    <td style="padding:15px">Returns a paginated list of the pull request's activity log.
This includes comments that were made by the reviewers, updates and
approvals.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/pullrequests/activity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugPullrequestsPullRequestId(username, repoSlug, pullRequestId, callback)</td>
    <td style="padding:15px">Returns the specified pull request.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/pullrequests/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRepositoriesUsernameRepoSlugPullrequestsPullRequestId(username, repoSlug, pullRequestId, body, callback)</td>
    <td style="padding:15px">Mutates the specified pull request.
This can be used to change the pull request's branches or description.
Only open pull requests can be mutated.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/pullrequests/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdActivity(username, repoSlug, pullRequestId, callback)</td>
    <td style="padding:15px">Returns a paginated list of the pull request's activity log.
This includes comments that were made by the reviewers, updates and
approvals.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/pullrequests/{pathv3}/activity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRepositoriesUsernameRepoSlugPullrequestsPullRequestIdApprove(username, pullRequestId, repoSlug, callback)</td>
    <td style="padding:15px">Redact the authenticated user's approval of the specified pull
request.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/pullrequests/{pathv3}/approve?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRepositoriesUsernameRepoSlugPullrequestsPullRequestIdApprove(username, pullRequestId, repoSlug, callback)</td>
    <td style="padding:15px">Approve the specified pull request as the authenticated user.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/pullrequests/{pathv3}/approve?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdComments(username, pullRequestId, repoSlug, callback)</td>
    <td style="padding:15px">Returns a paginated list of the pull request's comments.
This includes both global, inline comments and replies.
The default sorting is oldest to newest and can be overridden with
the `sort` query parameter.
This endpoint also supports filtering and sorting of the results. See
[filtering and sorting](../../../../../../meta/filtering) for more
details.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/pullrequests/{pathv3}/comments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommentsCommentId(username, pullRequestId, commentId, repoSlug, callback)</td>
    <td style="padding:15px">Returns a specific pull request comment.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/pullrequests/{pathv3}/comments/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdCommits(username, pullRequestId, repoSlug, callback)</td>
    <td style="padding:15px">Returns a paginated list of the pull request's commits.
These are the commits that are being merged into the destination
branch when the pull requests gets accepted.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/pullrequests/{pathv3}/commits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRepositoriesUsernameRepoSlugPullrequestsPullRequestIdDecline(username, pullRequestId, repoSlug, callback)</td>
    <td style="padding:15px">Declines the pull request.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/pullrequests/{pathv3}/decline?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdDiff(username, pullRequestId, repoSlug, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/pullrequests/{pathv3}/diff?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRepositoriesUsernameRepoSlugPullrequestsPullRequestIdMerge(username, pullRequestId, repoSlug, body, callback)</td>
    <td style="padding:15px">Merges the pull request.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/pullrequests/{pathv3}/merge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugPullrequestsPullRequestIdPatch(username, pullRequestId, repoSlug, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/pullrequests/{pathv3}/patch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugDownloads(username, repoSlug, callback)</td>
    <td style="padding:15px">Returns a list of download links associated with the repository.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/downloads?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRepositoriesUsernameRepoSlugDownloads(username, repoSlug, callback)</td>
    <td style="padding:15px">Upload new download artifacts.
To upload files, perform a `multipart/form-data` POST containing one
or more `files` fields:
    $ echo Hello World > hello.txt
    $ curl -s -u evzijst -X POST https://api.bitbucket.org/2.0/repositories/evzijst/git-tests/downloads -F files=@hello.txt
When a file is uploaded with the same name as an existing artifact,
then the existing file will be replaced.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/downloads?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRepositoriesUsernameRepoSlugDownloadsFilename(username, filename, repoSlug, callback)</td>
    <td style="padding:15px">Deletes the specified download artifact from the repository.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/downloads/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugDownloadsFilename(username, filename, repoSlug, callback)</td>
    <td style="padding:15px">Return a redirect to the contents of a download artifact.
This endpoint returns the actual file contents and not the artifact's
metadata.
    $ curl -s -L https://api.bitbucket.org/2.0/repositories/evzijst/git-tests/downloads/hello.txt
    Hello World</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/downloads/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPipelinesForRepository(username, repoSlug, callback)</td>
    <td style="padding:15px">Find pipelines</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/pipelines/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPipelineForRepository(username, repoSlug, body, callback)</td>
    <td style="padding:15px">Endpoint to create and initiate a pipeline.
There are a couple of different options to initiate a pipeline, where the payload of the request will determine which type of pipeline will be instantiated.
# Trigger a Pipeline for a branch or tag
One way to trigger pipelines is by specifying the reference for which you want to trigger a pipeline (e.g. a branch or tag).
The specified reference will be used to determine which pipeline definition from the `bitbucket-pipelines.yml` file will be applied...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/pipelines/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPipelineForRepository(username, repoSlug, pipelineUuid, callback)</td>
    <td style="padding:15px">Retrieve a specified pipeline</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/pipelines/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPipelineStepsForRepository(username, repoSlug, pipelineUuid, callback)</td>
    <td style="padding:15px">Find steps for the given pipeline.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/pipelines/{pathv3}/steps/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPipelineStepForRepository(username, repoSlug, pipelineUuid, stepUuid, callback)</td>
    <td style="padding:15px">Retrieve a given step of a pipeline.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/pipelines/{pathv3}/steps/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPipelineStepLogForRepository(username, repoSlug, pipelineUuid, stepUuid, callback)</td>
    <td style="padding:15px">Retrieve the log file for a given step of a pipeline.
This endpoint supports (and encourages!) the use of [HTTP Range requests](https://tools.ietf.org/html/rfc7233) to deal with potentially very large log files.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/pipelines/{pathv3}/steps/{pathv4}/log?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">stopPipeline(username, repoSlug, pipelineUuid, callback)</td>
    <td style="padding:15px">Signal the stop of a pipeline and all of its steps that not have completed yet.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/pipelines/{pathv3}/stopPipeline?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoryPipelineConfig(username, repoSlug, callback)</td>
    <td style="padding:15px">Retrieve the repository pipelines configuration.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/pipelines_config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRepositoryPipelineConfig(username, repoSlug, body, callback)</td>
    <td style="padding:15px">Update the pipelines configuration for a repository.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/pipelines_config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRepositoryPipelineKeyPair(username, repoSlug, callback)</td>
    <td style="padding:15px">Delete the repository SSH key pair.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/pipelines_config/ssh/key_pair?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoryPipelineSshKeyPair(username, repoSlug, callback)</td>
    <td style="padding:15px">Retrieve the repository SSH key pair excluding the SSH private key. The private key is a write only field and will never be exposed in the logs or the REST API.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/pipelines_config/ssh/key_pair?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRepositoryPipelineKeyPair(username, repoSlug, body, callback)</td>
    <td style="padding:15px">Create or update the repository SSH key pair. The private key will be set as a default SSH identity in your build container.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/pipelines_config/ssh/key_pair?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoryPipelineKnownHosts(username, repoSlug, callback)</td>
    <td style="padding:15px">Find repository level known hosts.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/pipelines_config/ssh/known_hosts/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRepositoryPipelineKnownHost(username, repoSlug, body, callback)</td>
    <td style="padding:15px">Create a repository level known host.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/pipelines_config/ssh/known_hosts/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRepositoryPipelineKnownHost(username, repoSlug, knownHostUuid, callback)</td>
    <td style="padding:15px">Delete a repository level known host.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/pipelines_config/ssh/known_hosts/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoryPipelineKnownHost(username, repoSlug, knownHostUuid, callback)</td>
    <td style="padding:15px">Retrieve a repository level known host.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/pipelines_config/ssh/known_hosts/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRepositoryPipelineKnownHost(username, repoSlug, knownHostUuid, body, callback)</td>
    <td style="padding:15px">Update a repository level known host.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/pipelines_config/ssh/known_hosts/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoryPipelineVariables(username, repoSlug, callback)</td>
    <td style="padding:15px">Find repository level variables.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/pipelines_config/variables/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRepositoryPipelineVariable(username, repoSlug, body, callback)</td>
    <td style="padding:15px">Create a repository level variable.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/pipelines_config/variables/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRepositoryPipelineVariable(username, repoSlug, variableUuid, callback)</td>
    <td style="padding:15px">Delete a repository level variable.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/pipelines_config/variables/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoryPipelineVariable(username, repoSlug, variableUuid, callback)</td>
    <td style="padding:15px">Retrieve a repository level variable.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/pipelines_config/variables/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRepositoryPipelineVariable(username, repoSlug, variableUuid, body, callback)</td>
    <td style="padding:15px">Update a repository level variable.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/pipelines_config/variables/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPipelineVariablesForTeam(username, callback)</td>
    <td style="padding:15px">Find account level variables.</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/pipelines_config/variables/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPipelineVariableForTeam(username, body, callback)</td>
    <td style="padding:15px">Create an account level variable.</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/pipelines_config/variables/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePipelineVariableForTeam(username, variableUuid, callback)</td>
    <td style="padding:15px">Delete a team level variable.</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/pipelines_config/variables/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPipelineVariableForTeam(username, variableUuid, callback)</td>
    <td style="padding:15px">Retrieve a team level variable.</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/pipelines_config/variables/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePipelineVariableForTeam(username, variableUuid, body, callback)</td>
    <td style="padding:15px">Update a team level variable.</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/pipelines_config/variables/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPipelineVariablesForUser(username, callback)</td>
    <td style="padding:15px">Find user level variables.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/pipelines_config/variables/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPipelineVariableForUser(username, body, callback)</td>
    <td style="padding:15px">Create a user level variable.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/pipelines_config/variables/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePipelineVariableForUser(username, variableUuid, callback)</td>
    <td style="padding:15px">Delete an account level variable.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/pipelines_config/variables/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPipelineVariableForUser(username, variableUuid, callback)</td>
    <td style="padding:15px">Retrieve a user level variable.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/pipelines_config/variables/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePipelineVariableForUser(username, variableUuid, body, callback)</td>
    <td style="padding:15px">Update a user level variable.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/pipelines_config/variables/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugRefs(username, repoSlug, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/refs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugRefsBranches(username, repoSlug, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/refs/branches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugRefsBranchesName(username, name, repoSlug, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/refs/branches/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugRefsTags(username, repoSlug, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/refs/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRepositoriesUsernameRepoSlugRefsTags(username, repoSlug, body, callback)</td>
    <td style="padding:15px">Creates a new tag in the specified repository.
The payload of the POST should consist of a JSON document that
contains the name of the tag and the target hash.
```
{
    "name" : "new tag name",
    "target" : {
        "hash" : "target commit hash",
    }
}
```
This endpoint does support using short hash prefixes for the commit
hash, but it may return a 400 response if the provided prefix is
ambiguous. Using a full commit hash is the preferred approach.</td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/refs/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesUsernameRepoSlugRefsTagsName(username, name, repoSlug, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/repositories/{pathv1}/{pathv2}/refs/tags/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSnippets(role = 'owner', callback)</td>
    <td style="padding:15px">Returns all snippets. Like pull requests, repositories and teams, the
full set of snippets is defined by what the current user has access to.
This includes all snippets owned by the current user, but also all snippets
owned by any of the teams the user is a member of, or snippets by other
users that the current user is either watching or has collaborated on (for
instance by commenting on it).
To limit the set of returned snippets, apply the
`?role=[owner|contributor|member]` query parameter wher...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/snippets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSnippets(body, callback)</td>
    <td style="padding:15px">Creates a new snippet under the authenticated user's account.
Snippets can contain multiple files. Both text and binary files are
supported.
The simplest way to create a new snippet from a local file:
    $ curl -u username:password -X POST https://api.bitbucket.org/2.0/snippets               -F file=@image.png
Creating snippets through curl has a few limitations and so let's look
at a more complicated scenario.
Snippets are created with a multipart POST. Both `multipart/form-data`
and `multipar...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/snippets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSnippetsUsername(role = 'owner', username, callback)</td>
    <td style="padding:15px">Identical to `/snippets`, except that the result is further filtered
by the snippet owner and only those that are owned by `{username}` are
returned.</td>
    <td style="padding:15px">{base_path}/{version}/snippets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSnippetsUsername(username, body, callback)</td>
    <td style="padding:15px">Identical to `/snippets`, except that the new snippet will be
created under the account specified in the path parameter `{username}`.</td>
    <td style="padding:15px">{base_path}/{version}/snippets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSnippetsUsernameEncodedId(username, encodedId, callback)</td>
    <td style="padding:15px">Deletes a snippet and returns an empty response.</td>
    <td style="padding:15px">{base_path}/{version}/snippets/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSnippetsUsernameEncodedId(username, encodedId, callback)</td>
    <td style="padding:15px">Retrieves a single snippet.
Snippets support multiple content types:
* application/json
* multipart/related
* multipart/form-data
application/json
----------------
The default content type of the response is `application/json`.
Since JSON is always `utf-8`, it cannot reliably contain file contents
for files that are not text. Therefore, JSON snippet documents only
contain the filename and links to the file contents.
This means that in order to retrieve all parts of a snippet, N+1
requests need t...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/snippets/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putSnippetsUsernameEncodedId(username, encodedId, callback)</td>
    <td style="padding:15px">Used to update a snippet. Use this to add and delete files and to
change a snippet's title.
To update a snippet, one can either PUT a full snapshot, or only the
parts that need to be changed.
The contract for PUT on this API is that properties missing from the
request remain untouched so that snippets can be efficiently
manipulated with differential payloads.
To delete a property (e.g. the title, or a file), include its name in
the request, but omit its value (use `null`).
As in Git, explicit re...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/snippets/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSnippetsUsernameEncodedIdComments(username, encodedId, callback)</td>
    <td style="padding:15px">Used to retrieve a paginated list of all comments for a specific
snippet.
This resource works identical to commit and pull request comments.
The default sorting is oldest to newest and can be overridden with
the `sort` query parameter.</td>
    <td style="padding:15px">{base_path}/{version}/snippets/{pathv1}/{pathv2}/comments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSnippetsUsernameEncodedIdComments(username, encodedId, body, callback)</td>
    <td style="padding:15px">Creates a new comment.
The only required field in the body is `content.raw`.
To create a threaded reply to an existing comment, include `parent.id`.</td>
    <td style="padding:15px">{base_path}/{version}/snippets/{pathv1}/{pathv2}/comments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSnippetsUsernameEncodedIdCommentsCommentId(username, commentId, encodedId, callback)</td>
    <td style="padding:15px">Deletes a snippet comment.
Comments can only be removed by their author.</td>
    <td style="padding:15px">{base_path}/{version}/snippets/{pathv1}/{pathv2}/comments/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSnippetsUsernameEncodedIdCommentsCommentId(username, commentId, encodedId, callback)</td>
    <td style="padding:15px">Returns the specific snippet comment.</td>
    <td style="padding:15px">{base_path}/{version}/snippets/{pathv1}/{pathv2}/comments/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putSnippetsUsernameEncodedIdCommentsCommentId(username, commentId, encodedId, callback)</td>
    <td style="padding:15px">Updates a comment.
Comments can only be updated by their author.</td>
    <td style="padding:15px">{base_path}/{version}/snippets/{pathv1}/{pathv2}/comments/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSnippetsUsernameEncodedIdCommits(username, encodedId, callback)</td>
    <td style="padding:15px">Returns the changes (commits) made on this snippet.</td>
    <td style="padding:15px">{base_path}/{version}/snippets/{pathv1}/{pathv2}/commits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSnippetsUsernameEncodedIdCommitsRevision(username, encodedId, revision, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/snippets/{pathv1}/{pathv2}/commits/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSnippetsUsernameEncodedIdWatch(username, encodedId, callback)</td>
    <td style="padding:15px">Used to stop watching a specific snippet. Returns 204 (No Content)
to indicate success.</td>
    <td style="padding:15px">{base_path}/{version}/snippets/{pathv1}/{pathv2}/watch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSnippetsUsernameEncodedIdWatch(username, encodedId, callback)</td>
    <td style="padding:15px">Used to check if the current user is watching a specific snippet.
Returns 204 (No Content) if the user is watching the snippet and 404 if
not.
Hitting this endpoint anonymously always returns a 404.</td>
    <td style="padding:15px">{base_path}/{version}/snippets/{pathv1}/{pathv2}/watch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putSnippetsUsernameEncodedIdWatch(username, encodedId, callback)</td>
    <td style="padding:15px">Used to start watching a specific snippet. Returns 204 (No Content).</td>
    <td style="padding:15px">{base_path}/{version}/snippets/{pathv1}/{pathv2}/watch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSnippetsUsernameEncodedIdWatchers(username, encodedId, callback)</td>
    <td style="padding:15px">Returns a paginated list of all users watching a specific snippet.</td>
    <td style="padding:15px">{base_path}/{version}/snippets/{pathv1}/{pathv2}/watchers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSnippetsUsernameEncodedIdNodeId(username, nodeId, encodedId, callback)</td>
    <td style="padding:15px">Deletes the snippet.
Note that this only works for versioned URLs that point to the latest
commit of the snippet. Pointing to an older commit results in a 405
status code.
To delete a snippet, regardless of whether or not concurrent changes
are being made to it, use `DELETE /snippets/{encoded_id}` instead.</td>
    <td style="padding:15px">{base_path}/{version}/snippets/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSnippetsUsernameEncodedIdNodeId(username, encodedId, nodeId, callback)</td>
    <td style="padding:15px">Identical to `GET /snippets/encoded_id`, except that this endpoint
can be used to retrieve the contents of the snippet as it was at an
older revision, while `/snippets/encoded_id` always returns the
snippet's current revision.
Note that only the snippet's file contents are versioned, not its
meta data properties like the title.
Other than that, the two endpoints are identical in behavior.</td>
    <td style="padding:15px">{base_path}/{version}/snippets/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putSnippetsUsernameEncodedIdNodeId(username, encodedId, nodeId, callback)</td>
    <td style="padding:15px">Identical to `UPDATE /snippets/encoded_id`, except that this endpoint
takes an explicit commit revision. Only the snippet's "HEAD"/"tip"
(most recent) version can be updated and requests on all other,
older revisions fail by returning a 405 status.
Usage of this endpoint over the unrestricted `/snippets/encoded_id`
could be desired if the caller wants to be sure no concurrent
modifications have taken place between the moment of the UPDATE
request and the original GET.
This can be considered a so...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/snippets/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSnippetsUsernameEncodedIdNodeIdFilesPath(username, pathParam, nodeId, encodedId, callback)</td>
    <td style="padding:15px">Retrieves the raw contents of a specific file in the snippet. The
`Content-Disposition` header will be "attachment" to avoid issues with
malevolent executable files.
The file's mime type is derived from its filename and returned in the
`Content-Type` header.
Note that for text files, no character encoding is included as part of
the content type.</td>
    <td style="padding:15px">{base_path}/{version}/snippets/{pathv1}/{pathv2}/{pathv3}/files/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSnippetsUsernameEncodedIdRevisionDiff(username, pathParam, encodedId, revision, callback)</td>
    <td style="padding:15px">Returns the diff of the specified commit against its first parent.
Note that this resource is different in functionality from the `patch`
resource.
The differences between a diff and a patch are:
* patches have a commit header with the username, message, etc
* diffs support the optional `path=foo/bar.py` query param to filter the
  diff to just that one file diff (not supported for patches)
* for a merge, the diff will show the diff between the merge commit and
  its first parent (identical to h...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/snippets/{pathv1}/{pathv2}/{pathv3}/diff?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSnippetsUsernameEncodedIdRevisionPatch(username, encodedId, revision, callback)</td>
    <td style="padding:15px">Returns the patch of the specified commit against its first
parent.
Note that this resource is different in functionality from the `diff`
resource.
The differences between a diff and a patch are:
* patches have a commit header with the username, message, etc
* diffs support the optional `path=foo/bar.py` query param to filter the
  diff to just that one file diff (not supported for patches)
* for a merge, the diff will show the diff between the merge commit and
  its first parent (identical to h...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/snippets/{pathv1}/{pathv2}/{pathv3}/patch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTeamsOwnerProjects(owner, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/projects/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTeamsOwnerProjects(owner, body, callback)</td>
    <td style="padding:15px">Creates a new project.
Note that the avatar has to be embedded as either a data-url
or a URL to an external image as shown in the examples below:
```
$ body=$(cat << EOF
{
    "name": "Mars Project",
    "key": "MARS",
    "description": "Software for colonizing mars.",
    "links": {
        "avatar": {
            "href": "data:image/gif;base64,R0lGODlhEAAQAMQAAORHHOVSKudfOulrSOp3WOyDZu6QdvCchPGolfO0o/..."
        }
    },
    "is_private": false
}
EOF
)
$ curl -H "Content-Type: application/js...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/projects/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTeamsOwnerProjectsProjectKey(owner, projectKey, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/projects/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTeamsOwnerProjectsProjectKey(owner, projectKey, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/projects/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTeamsOwnerProjectsProjectKey(owner, projectKey, body, callback)</td>
    <td style="padding:15px">Since this endpoint can be used to both update and to create a
project, the request body depends on the intent.
### Creation
See the POST documentation for the project collection for an
example of the request body.
Note: The `key` should not be specified in the body of request
(since it is already present in the URL). The `name` is required,
everything else is optional.
### Update
See the POST documentation for the project collection for an
example of the request body.
Note: The key is not requi...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/teams/{pathv1}/projects/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
